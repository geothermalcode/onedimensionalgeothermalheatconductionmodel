from nose import with_setup
from nose.tools import assert_equals, assert_raises, raises, assert_not_equals
from StratColumnIterator import StratColumnIterator as SCI
from CsvReader import read_wells
import pandas as pd
import numpy as np                  

full_StratFrame = None
test_StratFrame = None
dummy = None
test_basement_conductivity = None   
test_WellSedThick = None

def SCI_setup_function():
    """Setup a few constant thingies for testing StratColumnIterator"""
    global full_StratFrame
    global test_StratFrame
    global dummy
    global test_basement_conductivity
    global test_WellSedThick
    
    dummy = {'Assumed':[10.,20.,30.], 'Conductivity':[1.0,2.0,3.0]}
    test_StratFrame = pd.DataFrame(dummy)
    full_StratFrame = read_wells(file='StratCol21.csv')
    test_basement_conductivity = 2.5
    test_WellSedThick = 60.
    
def teardown_function():
    """Teardown the matching constants from setup..."""
    
    global full_StratFrame
    global test_StratFrame
    global dummy
    global test_basement_conductivity
    global test_WellSedThick
    
    del test_basement_conductivity
    del full_StratFrame
    del test_StratFrame
    del dummy
    del test_WellSedThick
    
@raises(ValueError)
@with_setup(SCI_setup_function,teardown_function)
def testStratColumnIteratorWellSedThickErrorCheck():
    i = 0
    for thick,cond in SCI(test_StratFrame,60.):
        i += 1

@with_setup(SCI_setup_function,teardown_function)
def testStratColsShape():
    i = 0
    for thick,cond in SCI(test_StratFrame,60.,WellSedThick=test_WellSedThick):
        assert_equals(thick,dummy['Assumed'][i])
        assert_equals(cond,dummy['Conductivity'][i])
        i += 1
    
    
@with_setup(SCI_setup_function,teardown_function)
def testStratColumnIteratorPartialColumn1():
    i = 0
    for thick,cond in SCI(test_StratFrame,20.,WellSedThick=test_WellSedThick):
        if i == 0:
            assert_equals(thick, dummy['Assumed'][i])
            assert_equals(cond,dummy['Conductivity'][i])
            i += 1
        else:
            assert_equals(thick, 10.)
            assert_equals(cond,dummy['Conductivity'][i])

@with_setup(SCI_setup_function,teardown_function)
def testStratColumnIteratorPartialColumn2():
    i = 0
    for thick,cond in SCI(test_StratFrame,40.,WellSedThick=test_WellSedThick):
        if i <= 1:
            assert_equals(thick, dummy['Assumed'][i])
            assert_equals(cond,dummy['Conductivity'][i])
            i += 1
        else:
            assert_equals(thick, 10.)
            assert_equals(cond,dummy['Conductivity'][i])

@with_setup(SCI_setup_function,teardown_function)
def testStratColumnIteratorPartialColumn3():
    i = 0
    for thick,cond in SCI(test_StratFrame,2.,WellSedThick=test_WellSedThick):
        assert_equals(thick, 2.)
        assert_equals(cond,dummy['Conductivity'][i])
        i+=1

@with_setup(SCI_setup_function,teardown_function)
def testStratColumnIteratorTopOfColumn():
    i = 0
    for thick,cond in SCI(test_StratFrame,0.,WellSedThick=test_WellSedThick):
        assert_equals(thick, 0.)
        assert_equals(cond,dummy['Conductivity'][i])
        i+=1
        
@with_setup(SCI_setup_function,teardown_function)
def testStratColumnIteratorBelowBottomOfColumn():
    i = 0
    for thick,cond in SCI(test_StratFrame,100.,WellSedThick=test_WellSedThick,basement_cond = test_basement_conductivity):
        if i < 3:
            assert_equals(thick, dummy['Assumed'][i])
            assert_equals(cond,dummy['Conductivity'][i])
            i+=1
        else:
            assert_equals(thick,40.)
            assert_equals(cond,test_basement_conductivity)

@with_setup(SCI_setup_function,teardown_function)
def testStratColumnIteratorBoundary1():
    i = 0
    for thick,cond in SCI(test_StratFrame,10.,WellSedThick=test_WellSedThick):
        assert_equals(thick, 10.)
        assert_equals(cond,dummy['Conductivity'][i])
        i+=1
    assert_equals(i,1)

@with_setup(SCI_setup_function,teardown_function)
def testStratColumnIteratorBoundaryOfBasement():
    i = 0
    for thick,cond in SCI(test_StratFrame,60.,WellSedThick=test_WellSedThick,basement_cond = test_basement_conductivity):
        if i < 3:
            assert_equals(thick, dummy['Assumed'][i])
            assert_equals(cond,dummy['Conductivity'][i])
            i+=1
    # This is testing that the iterator does not return another
    # zero thickness layer at the bottom
    assert_equals(i,3)
    assert_equals(thick,dummy['Assumed'][-1])
    assert_equals(cond,dummy['Conductivity'][-1])

@with_setup(SCI_setup_function,teardown_function)
def testStratColumnIteratorUnneededBasementCond():
    i = 0
    for thick,cond in SCI(test_StratFrame,40.,WellSedThick=test_WellSedThick,basement_cond = test_basement_conductivity):
        assert_equals(cond,dummy['Conductivity'][i])
        if i < 2:
            assert_equals(thick, dummy['Assumed'][i])
            i+=1
        else:
            assert_equals(thick, 10)
    
    # This is testing that the iterator does not return another
    # zero thickness layer at the bottom
    assert_not_equals(cond,test_basement_conductivity)
    
@with_setup(SCI_setup_function,teardown_function)
def testStratColumnIteratorColumnThickness():
    #Select a depth of interest greater than the well sediment thickness. 
    #Therefore, 60 m is in the basement for the scaled COSUNA column,
    #and the total thickness should equal the depth of interest.
    depth_of_interest = 60.0
    tot_thick = 0.0
    test_WellSedThick = 40.
    for thick,cond in SCI(test_StratFrame, 
                          depth_of_interest=depth_of_interest, 
                          WellSedThick=test_WellSedThick, 
                          basement_cond = test_basement_conductivity):
        tot_thick += thick
    assert_equals(tot_thick,depth_of_interest)
    assert_equals(cond,test_basement_conductivity)
    
    #Select a depth of interest just inside the well sediment thickness. 
    #Therefore, 39 m is in the bottom layer of the stratigraphy.
    depth_of_interest = 39.0
    tot_thick = 0.0
    for thick,cond in SCI(test_StratFrame, 
                          depth_of_interest=depth_of_interest, 
                          WellSedThick=test_WellSedThick, 
                          basement_cond = test_basement_conductivity):
        tot_thick += thick
    assert_equals(tot_thick,depth_of_interest)
    assert_equals(cond,dummy['Conductivity'][-1])
    
if __name__ == '__main__':
    import nose
    nose.run(argv=[__file__, '--with-doctest', #'-v'\
                  ])