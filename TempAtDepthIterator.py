import numpy as np

'''
This function is used to return the temperature at a depth of interest

1) Retrieve the Depth at or above the desired Depth.
2) Retrieve the Depth below the desired Depth.
3) Linearly solve for the Temperature of the desired depth.

Fixme: It should be possible to specify a vector of depths and compute
the temperature at those depths exactly, without the need for iteration using 
the depths computed. A function can be added to this script to accomplish
this task. A very rough draft is provided.

'''

def TempAtDepth(Temps, DesiredDepth, Depths):
            #Convert to an array for faster operation
            Depths = np.array(Depths)

            #Check that the desired temperature is in Temps
            if DesiredDepth > Depths[len(Depths) - 1]:
                #If not, assign a value of -9999, indicating that the temperature was not reached in Temps
                Temp_at_DesiredDepth = -9999.
            elif DesiredDepth < Depths[0]:
                Temp_at_DesiredDepth = -9999.
            else:
                #Determine the index of the closest Depth to the desired depth
                idx_closest = (np.abs(Depths - DesiredDepth)).argmin()
                if Depths[idx_closest] >= DesiredDepth:
                        #The next closest value must be located at the prior index.
                        idx_next = idx_closest - 1
                        #Linearly solve for the depth to the temperature
                        Temp_at_DesiredDepth = ((Temps[idx_closest] - Temps[idx_next])/ \
                                                (Depths[idx_closest] - Depths[idx_next]))* \
                                                (DesiredDepth - Depths[idx_next]) \
                                                + Temps[idx_next]
                else:
                        #The next closest value is located at the next index.
                        idx_next = idx_closest + 1
                        #Linearly solve for the depth to the temperature.
                        Temp_at_DesiredDepth = ((Temps[idx_next] - Temps[idx_closest])/ \
                                                (Depths[idx_next] - Depths[idx_closest]))* \
                                                (DesiredDepth - Depths[idx_closest]) \
                                                + Temps[idx_closest]
             
            return Temp_at_DesiredDepth

def ExactTempAtDepth(DesiredDepth):
    Temp = 1.
    return Temp