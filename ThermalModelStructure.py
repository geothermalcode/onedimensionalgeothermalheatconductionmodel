from CsvReader import read_wells, compute_gradients, read_strats
from TempEstimator import SedTempEst, BaseTempEst
from ThermalConductivityHarmonicAverager import HarmAvg, LowerLayerHarmAvg
from StratColumnIterator import StratColumnIterator
from PartialStratColKEst import PartialStratColumn as PSC
from DepthToTempIterator import DepthToTemp
from TempAtDepthIterator import TempAtDepth
import pandas as pd
import numpy as np
import matplotlib as mpl
mpl.use('Agg')
from matplotlib import pyplot as plt
import math

"""
This is the main file that calls functions that calculate the surface heat flow
abd temperatures at depth. The model assumes that there are layer(s) of 
sedimentary rock above basement rocks (crust). The radiogenic heat production 
(RHP) in the basement is not known, and is approximated by an exponential decay 
in RHP that begins at the surface of the basement and ends at the Moho, which 
is taken as 3*B, where B is the log-decrement of the exponential decay. 
Knowledge of the RHP in sedimentary rocks is assumed to be known/approximated 
by, for instance, gamma ray logs. The heat generation is assumed to be a
constant value as specified by the user. Future versions may allow for layer
by layer specification of heat generation.

Assumes that the Wells input file has at least these 7 columns:
Qmantle [mW/m**2]  - heat flow at the Moho
SedRadHeat [uW/m**3] - RHP in sedimentary rocks
WellDepth [m] - depth of the BHT measurement
SurfTemp [C] - average annual surface temperature
BHT [C] - bottom hole temperature (corrected appropriately)
RowID_ [] - Unique well identifier
ROME_ID [] - to identify if a well is in the Rome Trough
COSUNA_ID [] - to identify the stratigraphic column
FIXME: add basement conductivity to input file

Assumes that the Strat input file contains at least these 2 columns
Assumed [m] - assumed thickness of the formation 
Conductivity [W/mK] - thermal conductivity of the formation
FIXME: Could also include the radiogenic heat production in each layer 
within this file, rather than in the wells file.

Units are converted within the code.

Warnings stating that
FutureWarning: comparison to `None` will result in an elementwise object
comparison in the future will appear. 
These can be ignored because they are being compared to
a single element only within an if statement. This is the intended use.

The save file name can be changed at the last line of code.
"""

#SET OPTION TO HAVE TEMPERATURES AT ALL DEPTHS IN THE OUTPUT TABLE
#Change to 1 if you want all temperatures and stratigraphic information for each well in
#the output table. This computation is not highly time intensive, but will increase the
#output file size greatly.
AllTempsThicksConds = 1

#SET CONSTANTS:
#Set the value of the assumed basement conductivity
basement_conductivity = 2.7 #W/mK

#READ WELL DATA:
#Read in well data. Skip the first row (0) if it has units.
Wells = read_wells(file='ExampleInputTable.csv',
                   skiprows=[],
                   dtypes={'RowID_':str})

#Delete nan rows from the read in data that may result from added rows at end of file:
if np.isnan(Wells['WellDepth'][len(Wells) - 1]):
        while np.isnan(Wells['WellDepth'][len(Wells) - 1]):
                Wells = Wells.iloc[:(len(Wells) - 1),]
#Delete nan columns if all values in the column are nan.
if all(np.isnan(Wells.iloc[:,(len(Wells.columns) - 1)])):
        while all(np.isnan(Wells.iloc[:,(len(Wells.columns) - 1)])):
            Wells = Wells.iloc[:,:(len(Wells.columns) - 1)]

#CONVERT UNITS:
#Convert from units of mW/m^2 to W/m^2
Wells['Qmantle'] /= 1000. 
#Convert ftom units of uW/m^3 to W/m^3
Wells['SedRadHeat'] /= 1.e6

#CALCULATE GRADIENTS:
compute_gradients(Wells)

#SET STORAGE LISTS:
'''Establish lists to store variables that will be returned
B - Log decrement of radiogenic heat generation in basement (m)
Ab - Heat generation at top of basement (uW/m**3)
Qs - Surface heat flow (mW/m**2)
BHT_diff - Difference in (predicted - measured) BHTs.
Temp_1 - list of all calculated temperatures
Depth50C - Depth to 50C (m)
Depth80C - Depth to 80C (m)
Depth100C - Depth to 100C (m)
Temp2km - Temperature at 2 km (m)
Temp3km - Temperature at 3 km (m)
Temp4km - Temperature at 4 km (m)
Temp5km - Temperature at 5 km (m)
well_PSC - Suite of calculated conductivity and thickness information for each well.
'''
Bs = []
Abs = []
Qss = []
BHT_diffs = []
Temp_1 = []
Depth50C = []
Depth80C = []
Depth100C = []
Temp2km = []
Temp3km = []
Temp4km = []
Temp5km = []
well_PSCs = []

#STORE VARIBALES FOR FIGURES
BHTs = []
BHT_depths = []

#Set starting depth for the calculations. Must be greater than 0 to prevent nans. (m)
start_depth = 10. #m
#Set the maximum depth to calculate temperature at depth (m)
MaxDepth = 5000. #m
#Set the depth increment for the calculations (m)
#Note that temperatures at a specific depths will be linearly interpolated based on
#these depth increments.
depth_increment = 10. #m

#Ensure that all WellDepth are deeper than the start_depth:
match_test = np.where(np.array(Wells['WellDepth']) < start_depth)
if len(np.array(match_test).tolist()[0]) > 0:
        #Return a list of wells that have WellDepth
        Return = Wells.iloc[np.array(match_test).tolist()[0],:]
        print("Some wells are shallower than the start_depth. Return contains these wells.")
        print(Return)
        import sys
        sys.exit("Some WellDepth are shallower than start_depth")
if start_depth == 0:
        print("start_depth must be greater than 0")
        import sys
        sys.exit("start_depth must be greater than 0")

#FIGURE SETUP:
#Set up figure for plotting Depth vs. BHTs
fig = plt.figure()
plt.gca().invert_yaxis()
plt.xlabel('Temperature (deg C)')
plt.ylabel('Depth (m)')
plt.ylim(MaxDepth, 0)
plt.yticks(np.arange(0,int(MaxDepth+1),500), range(0,int(MaxDepth+1),500))

#SET LOOP COUNTER VARIABLES: 
#Set the number of negative Ab values to 0, so that they may be counted.
num_neg_Ab = 0

#Counting for a progress bar in % of wells completed
count = 0.
#Set progress tracking starting value and reporting increment in percent.
progress_tracker = 10.
progress_increment = 10.

#Read in COSUNA stratigraphy files
CosunaKeys = [2701, 2801, 2901, 3001, 3101, 1701, 1801, 2101, 2201, 2301, 2401, 1201, 1301, 1702, 1802, 101, 201, 301, 801, 901, 1001, 401, 50101, 50102, 60101, 60102, 110101, 110102, 2102, 2202, 230201, 230202, 2402] 
CosunaFilenames = ['NY27.csv', 'NY28.csv', 'NY29.csv', 'NY30.csv', 'NY31.csv', 'PA17.csv', 'PA18.csv', 'PA21.csv', 'PA22.csv', 'PA23.csv', 'PA24.csv', 'MD12.csv', 'MD13.csv', 'KY17.csv', 'KY18.csv', 'WV1.csv', 'WV2.csv', 'WV3.csv', 'WV8.csv', 'WV9.csv', 'WV10.csv', 'VA4.csv', 'VA501.csv', 'VA502.csv', 'VA601.csv', 'VA602.csv', 'VA1101.csv', 'VA1102.csv', 'VA21.csv', 'VA22.csv', 'VA2301.csv', 'VA2302.csv', 'VA24.csv']
RomeKeys = [1701, 1801, 2101, 2201, 1702, 1802, 101, 201, 801, 901] 
RomeFilenames = ['PA17_RT.csv', 'PA18_RT.csv', 'PA21_RT.csv', 'PA22_RT.csv', 'KY17_RT.csv', 'KY18_RT.csv', 'WV1_RT.csv', 'WV2_RT.csv', 'WV8_RT.csv', 'WV9_RT.csv']
CosunaData = {}
RomeData = {}
read_strats(CosunaData, CosunaKeys, CosunaFilenames, 'Carter Conductivity')
read_strats(RomeData, RomeKeys, RomeFilenames, 'Carter Conductivity')

#These are compared to None in the loop, and should be declared before running.
Depths = None
Depths2 = None
Depths3 = None

#BEGIN TEMP @ DEPTH CALCS:
#Loop through all wells to determine surface heat flow (Qs), and Temp @ Depths
for well in Wells.iterrows():
    #Assign the input data to row
    row = well[1]
    
    #Store these values for figure generation
    BHTs.append(row['BHT'])
    BHT_depths.append(row['WellDepth'])

    #READ STRAT COLUMN:
    if row['ROME_ID'] == 1:
            #Well is in the Rome Trough, use rome trough modified Cosuna column.
            Strat = RomeData[row['COSUNA_ID']]
    else:
            #Use regular Cosuna column
            Strat = CosunaData[row['COSUNA_ID']]
    
    #Set the value of the log decrement in basement radiogenic heat production
    #The value of B may change based on data collected from the region of study. 
    if row['BasementDepth'] > 3000.:
        B = 13000. - row['BasementDepth']
    else:
        B = 10000.

    Bs.append(B)
    
    #For all wells:
    #First determine the conductivity to the well depth
    #using the partial strat column (PSC) function.
    this_PSC = PSC(Strat,row['BasementDepth'],basement_conductivity,row['WellDepth'])
    well_PSCs.append(this_PSC)

    #Calculate the surface heat flow in the well
    '''The original equation approximates the heat flow using ONLY 
    information from the mantle to the well depth, where we have a control BHT 
    measurement. Essentially, the well depth is treated as a location of a 
    hypothetical hot temperature reservoir with a known 
    temperature (the BHT) that accounts for the heat flow and any 
    radiogenic heat production from the mantle up to the well depth.
    Thus, the original surface heat flow calculation assumes that this same 
    amount of heat flow at the well depth propagates upward to the surface, and  
    changes based ONLY on the change in the cold reservoir temperature 
    (the surface temperautre).
    That said, to be accurate to our assumption of sediment RHP, heat generated above
    the well depth should also be accounted for.
    '''
    #Originally: Traditional Approach
    # This approach implicitly assigns the value for the temperature gradient to the
    # surface and neglects any heat generation from the depth of the well to the surface.
    #Qs = this_PSC.Kw * row['Gradient']/1000.

    #Modified: Physically correct method
    ''' This approach calculates the surface heat flow by analytically solving for the
        heat flow from the temeprature at depth equation. For basement rocks, the heat
        balance must be inserted to solve for the surface heat flow
        (2 equations, and the 2 unknowns are Qs and Ab).
    '''

    if (row['WellDepth'] <= row['BasementDepth']):
            #Add the additional sedimentary rock heat generation from the surface to the well depth.
            Qs = ((row['BHT'] - row['SurfTemp'])*this_PSC.Kw + \
                  (row['SedRadHeat']*(row['WellDepth'])**2)/2.)/row['WellDepth']
    else:
            #Add additional heat generation from the basement and the sedimentary rocks.
            Qs = ((row['BHT'] - row['SurfTemp']) + \
                  ((row['SedRadHeat']*(row['BasementDepth'])**2)/(2.*this_PSC.Kc)) + \
                  (row['Qmantle']/((1. - np.exp(-3.))*basement_conductivity))*(B*(1. - np.exp(-(row['WellDepth'] - row['BasementDepth'])/B)) - (row['WellDepth'] - row['BasementDepth'])) + \
                  ((row['SedRadHeat']*row['BasementDepth'])/basement_conductivity)*((row['WellDepth'] - row['BasementDepth'])*(1. - 1./(1. - np.exp(-3.))) + B*(1. - np.exp(-(row['WellDepth'] - row['BasementDepth'])/B))/(1. - np.exp(-3.)))) / \
                  (row['BasementDepth']/this_PSC.Kc + (row['WellDepth'] - row['BasementDepth'])/basement_conductivity * (1. - 1./(1. - np.exp(-3.))) + \
                   B*(1. - np.exp(-(row['WellDepth'] - row['BasementDepth'])/B))/(basement_conductivity * (1. - np.exp(-3.))))

    #Store heat flow in mW/m^2     
    Qss.append(Qs*1.e3)
    
    #Calculate the heat generation at the sediment-basement contact.
    ''' FIXME: Calculated values of the basement heat generation using this formula 
               can be negative. Need to ensure that this is non-negative.
        FIXME: The np.exp(-3B/B) should really have a constant in the numerator representative of total
               crustal thickness in the region. The scaling of B above tries to get at the difference
               in crustal thickness based on changes in basin thickness, but this may not be the best approach.
    '''

    #Former method:
    #Ab = (Qs - row['SedRadHeat']*row['BasementDepth'] - row['Qmantle'])/B
    #New Method:
    Ab = (Qs - row['SedRadHeat']*row['BasementDepth'] - row['Qmantle']) \
        /(B*(1. - np.exp(-3.)))
    
    # Kludge! We are setting negative Ab's to zero here
    #FIXME: Reducing the Ab value and not adjusting another variable, 
    # such as mantle heat flow (below), upsets the heat balance.
    # As a result, temperatures at depth in the basement will be greater than 
    # they really are for these wells.
    if Ab < 0.: 
        Ab = 0.
        num_neg_Ab += 1
    #One could take this approach and decrease Qmantle, then recompute Ab (which will now equal 0).
    # but in tests this can cause Qmantle to become unreasonably low or even negative. 
    # It's possible that these wells provide bad BHT-depth data, or are not representative of heat conduction.
    #if Ab < 0.:
    #    if row['WellDepth'] <= row['BasementDepth']:
            #Reduce the mantle heat flow and then recalculate Ab. Ab should now equal 0.
    #        Qmantle_adjusted = row['Qmantle'] + Ab*B*(1. - np.exp(-3.))
    #        Ab = (Qs - row['SedRadHeat']*row['BasementDepth'] - Qmantle_adjusted) \
    #                 /(B*(1. - np.exp(-3.)))       
    #    if Ab < 0.:
    #        Ab = 0.
    #        num_neg_Ab += 1
    
    #Store basement radiogenic heat production at the sediment-basement contact in uW/m^3
    Abs.append(Ab*1.e6)

    #Begin Temperature Estimations
    Temps = []

    #Calculate temperature up until the well depth or the MaxDepth
    #This will always stop at or shallower than the well depth or MaxDepth. The 
    #'stop' value is not included in the increment, so a very small number is 
    #added so that the well depth or MaxDepth is included in the interval. 
    if row['WellDepth'] <= MaxDepth:
            #Can estimate to the well depth.
            Depths = np.arange(start_depth,
                               row['WellDepth'] + 0.00001,  # keep WellDepth if integer multiple of increment.
                               depth_increment)
    else:
            #well is deeper than the maximum depth. Only go to the maximum depth.
            Depths = np.arange(start_depth,
                               MaxDepth + 0.00001,  # keep MaxDepth if integer multiple of increment.
                               depth_increment)
    
    if row['WellDepth'] <= row['BasementDepth']:
        for z in Depths:
            #We're above the basement, and since we are setting "Depths" 
            #up to the well depth, we must be estimating shallower than 
            #the well depth, which is the "Upper" strat column:
            K = this_PSC.EstimateKForUpperPartialStratColumn(z)
                                           
            Temp = row['SurfTemp'] + SedTempEst(K, z, Qs, row['SedRadHeat'])

            Temps.append(Temp)

    else:
        #Well penetrates the basement
        for z in Depths:
            if z <= row['BasementDepth']:
                #We're in the sediments, so use the "Upper" strat column, 
                #same as above:
                K = this_PSC.EstimateKForUpperPartialStratColumn(z)
                                           
                Temp = row['SurfTemp'] + SedTempEst(K, z, Qs, row['SedRadHeat'])

                Temps.append(Temp)
                
            else:
                #We're in the basement, so this requires 2 conductivities. 
                #The first is Kc for the sediments (K here), and the second is
                #the basement conductivity, which is set above as a constant.
                K = this_PSC.EstimateKForUpperPartialStratColumn(row['BasementDepth'])
                
                #The z in SedTempEst is row['BasementDepth'] because we are
                #estimating to the extent of the sediment here. 
                #The z in BaseTempEst is (z-row['BasementDepth']),
                #as per function design.
                Temp = row['SurfTemp'] + \
                       SedTempEst(K,
                                  row['BasementDepth'],
                                  Qs, row['SedRadHeat']) + \
                       BaseTempEst(basement_conductivity,
                                   (z-row['BasementDepth']),
                                   row['Qmantle'],
                                   Ab,
                                   B)

                Temps.append(Temp)
                

    #Calculate the temperature at the well depth. This will equal the BHT
    #for all wells except those that are in the basement AND Ab was forced to 0.
    if row['WellDepth'] <= row['BasementDepth']:
            BHT_calc = row['SurfTemp'] + \
                       SedTempEst(this_PSC.Kw,
                                  this_PSC.well_depth,
                                  Qs,
                                  row['SedRadHeat'])

    else:
            #Well is in the basement. 2 conductivities are needed: 
            #the average in the sediment column (K_sed_col) 
            #and the basement conductivity.
            K_sed_col = this_PSC.EstimateKForUpperPartialStratColumn(row['BasementDepth'])

            #Then the calculated BHT is: 
            #The z in BaseTempEst is (row['WellDepth']-row['BasementDepth']) 
            BHT_calc = row['SurfTemp'] + \
                       SedTempEst(K_sed_col,
                                  row['BasementDepth'],
                                  Qs,
                                  row['SedRadHeat']) + \
                       BaseTempEst(basement_conductivity,
                                   (this_PSC.well_depth - row['BasementDepth']),
                                   row['Qmantle'],
                                   Ab,
                                   B)
    
    #Check how far the calculated BHT is from the measured BHT
    #In Celsius: 
    BHT_diff = (BHT_calc - row['BHT'])
    BHT_diffs.append(BHT_diff)

    #Calculate the temperatures until the basement
    #Use this loop for estimating temperature at depths deeper than the 
    #well, but shallower than the basement.
    if Depths[-1] == MaxDepth:
            #Do Nothing. Already calculated temperature to the maximum desired depth
            Depths2 = None
    else:
            #There are more depths before MaxDepth.
            #Check if the last increment was deeper than the basement.
            if (Depths[-1]+depth_increment) <= row['BasementDepth']:
                    #Calculate the temperature up until the basement.
                    if row['BasementDepth'] <= MaxDepth:
                            #Top of Basement is shallower than MaxDepth.
                            #Calculate to Top of Basement.
                            Depths2 = np.arange(Depths[-1] + depth_increment,
                                                row['BasementDepth'] + 0.00001,
                                                depth_increment)
                    else:
                            #Basement is deeper than the maximum depth.
                            #Only calculate to MaxDepth.
                            Depths2 = np.arange(Depths[-1] + depth_increment,
                                                MaxDepth + 0.00001,
                                                depth_increment)
                            
                    for z2 in Depths2:
                            Kw, Kss, Kc = this_PSC.EstimateKForLowerPartialStratColumn(z2)
            
                            Temp = row['SurfTemp'] + \
                                   SedTempEst(Kw, 
                                             this_PSC.well_depth, 
                                             Qs, 
                                             row['SedRadHeat']) + \
                                   SedTempEst(Kss, 
                                             (z2 - this_PSC.well_depth), 
                                             (Qs - row['SedRadHeat']*this_PSC.well_depth), 
                                             row['SedRadHeat'])

                            Temps.append(Temp)

            else:
                    #The case when MaxDepth has not been reached, and the
                    #last depth in Depths was greater than the basement depth
                    #because the well was in the basement.
                    #Depths 2 not needed, so set to None.
                    Depths2 = None
     
    #Use this loop once estimating in the basement.
    if Depths[-1] == MaxDepth:
            #Do nothing. Already calculated to the maximum depth.
            Depths3 = None
    elif Depths2 is None:
            if Depths[-1] == MaxDepth:
                    #Do nothing. Already calculated to the maximum depth.
                    Depths3 = None
            else:              
                    #Calculate Depths 3
                    #Have not calculated to the MaxDepth. Continue to MaxDepth.
                    #Use the last element in Depths as the starting point
                    Depths3 = np.arange(Depths[-1] + depth_increment, 
                                        MaxDepth + 0.00001,
                                        depth_increment)
                            
                    #Assign conductivities for calculations.  
                    if row['WellDepth'] <= row['BasementDepth']:
                        Kss_column = (1./((this_PSC.basement_depth/this_PSC.Kc - this_PSC.well_depth/this_PSC.Kw)/(this_PSC.basement_depth - this_PSC.well_depth)))
                    else:
                        #Well penetrates the basement. This is Kc 
                        Kss_column = this_PSC.EstimateKForUpperPartialStratColumn(row['BasementDepth'])
        
                    if row['WellDepth'] <= row['BasementDepth']:
                        #Well shallower than basement.
                        for z3 in Depths3:
                            Temp = row['SurfTemp'] + \
                                   SedTempEst(this_PSC.Kw,  # To well depth in sediments
                                              this_PSC.well_depth, 
                                              Qs, 
                                              row['SedRadHeat']) + \
                                   SedTempEst(Kss_column, # Below well in remainder of sediments
                                              (this_PSC.basement_depth - this_PSC.well_depth), 
                                              (Qs - row['SedRadHeat']*this_PSC.well_depth), 
                                              row['SedRadHeat'])  + \
                                   BaseTempEst(basement_conductivity, # In the basement
                                              (z3 - this_PSC.basement_depth), 
                                              (row['Qmantle']), 
                                              Ab, 
                                              B)

                            Temps.append(Temp)
                            
                    else:
                        #Well is deeper than the basement
                        for z3 in Depths3:
                            Temp = row['SurfTemp'] + \
                                   SedTempEst(Kss_column,  # The entire sedimentary column
                                              row['BasementDepth'], 
                                              Qs, 
                                              row['SedRadHeat']) + \
                                   BaseTempEst(basement_conductivity, 
                                              (z3 - this_PSC.basement_depth), 
                                              (row['Qmantle']), 
                                              Ab, 
                                              B)  # The basement segment of the well

                            Temps.append(Temp)
                    
    elif Depths2[-1] == MaxDepth:
                    #Do nothing. Already calculated to maximum depth.
                    Depths3 = None
    else:
            #Have not calculated to the MaxDepth. Continue to MaxDepth.
            if row['WellDepth'] <= row['BasementDepth']:
                    #Use the last element in Depths2 as the starting point
                    Depths3 = np.arange(Depths2[-1] + depth_increment, 
                                        MaxDepth + 0.00001,
                                        depth_increment)
            else: 
                    #Use the last element in Depths as the starting point
                    Depths3 = np.arange(Depths[-1] + depth_increment, 
                                        MaxDepth + 0.00001,
                                        depth_increment)
    
            #Assign conductivities for calculations
            if row['WellDepth'] <= row['BasementDepth']:
                #This is Kss, below the well in the sediments.
                Kss_column = (1./((this_PSC.basement_depth/this_PSC.Kc - this_PSC.well_depth/this_PSC.Kw)/(this_PSC.basement_depth - this_PSC.well_depth)))
            else:
                #Well penetrates the basement. This is Kc
                Kss_column = this_PSC.EstimateKForUpperPartialStratColumn(row['BasementDepth'])

            #Calculate temperature to the maximum depth.    
            if row['WellDepth'] <= row['BasementDepth']:
                for z3 in Depths3:
                    Temp = row['SurfTemp'] + \
                           SedTempEst(this_PSC.Kw,  # To well depth in sediments
                                      this_PSC.well_depth, 
                                      Qs, 
                                      row['SedRadHeat']) + \
                           SedTempEst(Kss_column, # Below well in remainder of sediments
                                      (this_PSC.basement_depth - this_PSC.well_depth), 
                                      (Qs - row['SedRadHeat']*this_PSC.well_depth), 
                                      row['SedRadHeat'])  + \
                           BaseTempEst(basement_conductivity, # In the basement
                                      (z3 - this_PSC.basement_depth), 
                                      (row['Qmantle']), 
                                      Ab, 
                                      B)

                    Temps.append(Temp)
                    
            else:
                #Well is deeper than the basement
                for z3 in Depths3:
                    Temp = row['SurfTemp'] + \
                           SedTempEst(Kss_column,  # The entire sedimentary column
                                      row['BasementDepth'], 
                                      Qs, 
                                      row['SedRadHeat']) + \
                           BaseTempEst(basement_conductivity, 
                                      (z3 - this_PSC.basement_depth), 
                                      (row['Qmantle']), 
                                      Ab, 
                                      B)  # The basement segment of the well...

                    Temps.append(Temp)

    #Set up depths for plotting and storing data
    if Depths3 is None:
            if Depths2 is None:
                    all_depths = Depths
            else:
                    all_depths = np.concatenate((Depths, Depths2))
    else:
            if Depths2 is None:
                    all_depths = np.concatenate((Depths, Depths3))
            else:
                    all_depths = np.concatenate((Depths, Depths2, Depths3))
                    
    plt.set_cmap('jet')
    plt.plot(Temps,all_depths)

    #Determine the depth to temperatures and temps at depth:
    Depth_T50 = DepthToTemp(Temps = Temps, DesiredTemp = 50.,Depths = all_depths)
    Depth_T80 = DepthToTemp(Temps = Temps, DesiredTemp = 80.,Depths = all_depths)
    Depth_T100 = DepthToTemp(Temps = Temps, DesiredTemp = 100.,Depths = all_depths)
    Temp_2km = TempAtDepth(Temps = Temps, DesiredDepth = 2000., Depths = all_depths)
    Temp_3km = TempAtDepth(Temps = Temps, DesiredDepth = 3000., Depths = all_depths)
    Temp_4km = TempAtDepth(Temps = Temps, DesiredDepth = 4000., Depths = all_depths)
    Temp_5km = TempAtDepth(Temps = Temps, DesiredDepth = 5000., Depths = all_depths)
    
    Depth50C.append(Depth_T50)
    Depth80C.append(Depth_T80)
    Depth100C.append(Depth_T100)
    Temp2km.append(Temp_2km)
    Temp3km.append(Temp_3km)
    Temp4km.append(Temp_4km)
    Temp5km.append(Temp_5km)

    Temp_1.append(Temps)
    
    #Clear Variables for next loop run
    Strat = None
    B = None
    this_PSC = None
    Qs = None
    Ab = None
    BHT_diff = None
    BHT_calc = None
    K = None
    Kw = None
    Kss = None
    Kc = None
    Kss_column = None
    K_sed_col = None
    all_depths = None
    Depths = None
    Depths2 = None
    Depths3 = None
    Temp = None
    Depth_T50 = None
    Depth_T80 = None
    Depth_T100 = None
    Temp_2km = None
    Temp_3km = None
    Temp_4km = None
    Temp_5km = None

    #Send Progress Update in 10% increments as a percent of total wells run:
    count += 1.
    progress = count/len(Wells['RowID_'])*100
    if progress >= progress_tracker:
            print progress_tracker, '% done'
            progress_tracker += progress_increment
    

print('Writing output...')
#FIGURES:
#Temperature predictions (geotherms) at all depths. 
#Add BHT points to figure.
plt.scatter(BHTs,BHT_depths)
plt.savefig('well_curves.png')
plt.close(fig)

#Histogram of the BHT Errors.
fig1 = plt.figure()
plt.ylabel('Count')
plt.xlabel('Temperature Error (Calculated - Measured; deg C)')
plt.hist(BHT_diffs,bins=5)
plt.savefig('BHTErrorHist.png')
plt.close(fig1)

#Plot of BHT temps
fig2 = plt.figure()
plt.ylabel('Depth (m)')
plt.xlabel('BHT Measured; deg C)')
plt.scatter(BHTs,BHT_depths)
plt.gca().invert_yaxis()
plt.savefig('BHTDepthScatter.png')
plt.close(fig2)

pd.set_option('float_format', '{:10,.4g}'.format)

#STORE CALCULATED DATA
#Convert Qmantle and SedRadHeat back to original units
Wells['Qmantle'] *= 1000.
Wells['SedRadHeat'] *= 1.e6
# Add new columns for Qs, Ab, and the temps@depth (PSC) to Wells.
idx = Wells.index
Wells['Qs'] = pd.Series(Qss, index=idx)
Wells['Ab'] = pd.Series(Abs, index=idx)
Wells['B'] = pd.Series(Bs, index=idx)
#Store well-specific information. Use well_PSCs[idx].__dict__ to extract other information
Wells['Depth50C'] = pd.Series(Depth50C, index=idx)
Wells['Depth80C'] = pd.Series(Depth80C, index=idx)
Wells['Depth100C'] = pd.Series(Depth100C, index=idx)
Wells['Temp2km'] = pd.Series(Temp2km, index=idx)
Wells['Temp3km'] = pd.Series(Temp3km, index=idx)
Wells['Temp4km'] = pd.Series(Temp4km, index=idx)
Wells['Temp5km'] = pd.Series(Temp5km, index=idx)
Kw = []
Kc = []

#Extract information from the PSC stored data.
for i in idx:
        Kw.append(well_PSCs[i].Kw)
        Kc.append(well_PSCs[i].Kc)
Wells['Kw'] = pd.Series(Kw, index=idx)
Wells['Kc'] = pd.Series(Kc, index=idx)
Wells['BHT_Diff'] = pd.Series(BHT_diffs, index=idx)

if AllTempsThicksConds == 1:
        #Determine the length of the thickness and conductivity lists
        for i in range(len(CosunaData)):
                if i == 0:
                        arrlen = len(CosunaData[CosunaKeys[i]])
                else:
                        lenrow = len(CosunaData[CosunaKeys[i]])
                        arrlen = max(arrlen, lenrow)
        #Then check the RomeData columns
        for j in range(len(RomeData)):
                arrlen = max(arrlen, len(RomeData[RomeKeys[j]]))

        #Initialize the lists of Thicks and Conds to the column length of the max columns in Cosuna columns
        Thicks = [None]*arrlen
        Conds = [None]*arrlen

        #Extract thickness anf conductivity info.
        for i in idx:
                #Fill in the array of lithologic thickness and conductivity of each well
                if i == 0:
                        #Fill in the initialized row
                        Thicks[0:len(well_PSCs[i].Whole_Sediment_Column_Thick)] = well_PSCs[i].Whole_Sediment_Column_Thick
                        Conds[0:len(well_PSCs[i].Whole_Sediment_Column_Cond)] = well_PSCs[i].Whole_Sediment_Column_Cond
                else:
                        #Add a new row of None
                        Thicks = np.vstack([Thicks, [None]*arrlen])
                        Conds = np.vstack([Conds, [None]*arrlen])
                        #Add data to this row.
                        Thicks[i,0:len(well_PSCs[i].Whole_Sediment_Column_Thick)] = well_PSCs[i].Whole_Sediment_Column_Thick
                        Conds[i,0:len(well_PSCs[i].Whole_Sediment_Column_Cond)] = well_PSCs[i].Whole_Sediment_Column_Cond

        #Extract the information to the well database.
        #Make column names
        ColNamesTemps = ["T" + str(k) for k in (range(len(Temp_1[0]))[1:(len(range(len(Temp_1[0]))) + 1)] + [len(Temp_1[0])])]
        ColNamesThicks = ["Layer" + str(k) for k in (range(len(Thicks[0]))[1:(len(range(len(Thicks[0]))) + 1)] + [len(Thicks[0])])]
        ColNamesConds = ["Cond" + str(k) for k in (range(len(Conds[0]))[1:(len(range(len(Conds[0]))) + 1)] + [len(Conds[0])])]
        #Extract the information
        for j in range(len(Thicks[0])):
                Wells[ColNamesThicks[j]] = None
        for j in range(len(Conds[0])):
                Wells[ColNamesConds[j]] = None
        for j in range(len(Temp_1[0])):
                Wells[ColNamesTemps[j]] = None
        for j in range(len(Temp_1[0])):
                Wells[ColNamesTemps[j]] = np.array(Temp_1)[:,j]
        for j in range(len(Thicks[0])):
                Wells[ColNamesThicks[j]] = np.array(Thicks)[:,j]
        for j in range(len(Conds[0])):
                Wells[ColNamesConds[j]] = np.array(Conds)[:,j]

#PRINT DATA:
#print number of negative Ab values
print 'Number of negative Ab values =', num_neg_Ab

#SAVE DATA:
Wells.to_csv('ExampleOutputTable.csv')
print('Done.')