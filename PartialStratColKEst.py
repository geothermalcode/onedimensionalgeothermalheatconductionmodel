from StratColumnIterator import StratColumnIterator
from ThermalConductivityHarmonicAverager import HarmAvg, LowerLayerHarmAvg


class PartialStratColumn(object):
    """A class to deal with partial stratigraphic column computations. For
    example, to compute temperature at depth anywhere in the vertical column.
    
    Everything in this object is stretched to the "basement depth" and
    has the same "basement conductivity". In other words, this is appropriate
    for a single well with its corresponding COSUNA column, or known 
    stratigraphic column.
    """
    
    def __init__(self,Strat,basement_depth,basement_cond,well_depth):
        # Initialize by stashing away the Strat Data Frame and empty lists
        self.Strat = Strat
        self.basement_depth = basement_depth
        self.basement_cond = basement_cond
        self.well_depth = well_depth
        self.Whole_Sediment_Column_Thick = []
        self.Whole_Sediment_Column_Cond = []
        self.buildWholeColumn()
        self.Well_Column_Thick = []
        self.Well_Column_Cond = []
        self.buildWellColumn()
        
    def buildWholeColumn(self):
        # The stretch to a Cosuna depth is accomplished
        # within the StratColumnIterator
        # So we call this with the basement depth at the location of the well
        # both as the depth of interest and (obviously) the basement depth.
        for thick,cond in StratColumnIterator(self.Strat,
                                              self.basement_depth,
                                              WellSedThick=self.basement_depth,
                                              basement_cond=self.basement_cond):
            self.Whole_Sediment_Column_Thick.append(thick)
            self.Whole_Sediment_Column_Cond.append(cond)
            
            #This is the average conductivity in the sediment column because 
            #the StratColumnIterator and HarmAvg are called with the 
            #basement depth as the depth of interest.
            self.Kc = HarmAvg(self.Whole_Sediment_Column_Cond,
                              self.Whole_Sediment_Column_Thick,
                              self.basement_depth)

    def buildWellColumn(self):
        # If the well penetrates basement then the harmonic averager 
        # will not have that basement thickness. We need this function 
        # to retrieve the basement thickness and append it to the column.
        
        # The stretch to a Cosuna depth is accomplished within the StratColumnIterator
        # So we call this with the basement depth at the location of the well
        # as the basement_depth, but the well depth as the depth of interest.
        for thick,cond in StratColumnIterator(self.Strat,
                                              self.well_depth,
                                              WellSedThick=self.basement_depth,
                                              basement_cond=self.basement_cond):
            self.Well_Column_Thick.append(thick)
            self.Well_Column_Cond.append(cond)
            self.Kw = HarmAvg(self.Well_Column_Cond,
                              self.Well_Column_Thick,
                              self.well_depth)

        
        
    def EstimateKForUpperPartialStratColumn(self, z):
        # Builds the Thick and Cond arrays for the object
        # then estimates the Harmonic Average Conductivity up to depth z,
        # which is located in the sediments (NOT BASEMENT!).
        
        #Send the thicknesses and conductivities for the sediment column 
        #to the harmonic averager to determine the conductivity
        K = HarmAvg(self.Whole_Sediment_Column_Cond, 
                    self.Whole_Sediment_Column_Thick, z)
        return K


    def EstimateKForLowerPartialStratColumn(self,z):
        # This function is used when the conductivity below the
        # well depth (Kss) is needed. This also returns the average 
        # conductivity to the depth of interest (Kc = Kpc for partial strat col)
        # SEDIMENT ESTIMATION ONLY
        Kw, Kss, Kc = LowerLayerHarmAvg(self.Whole_Sediment_Column_Cond,
                                        self.Whole_Sediment_Column_Thick,
                                        z,
                                        self.well_depth,
                                        self.basement_cond,
                                        Kw=self.Kw)
        return Kw, Kss, Kc
    