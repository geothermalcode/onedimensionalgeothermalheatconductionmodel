from nose import with_setup
from nose.tools import assert_equals, assert_raises, raises, assert_almost_equal
import pandas as pd
from PartialStratColKEst import PartialStratColumn as PSC
import numpy as np

test_conductivities = None
test_thicknesses = None
test_basement_depth = None
test_well_depth = None
test_basement_cond = None
test_dfStrat = None
test_PSC = None


def PSCKE_setup_function():
    """Setup a few constant thingies for testing PartialStratColKEst"""
    global test_conductivities
    global test_thicknesses
    global test_basement_depth
    global test_well_depth
    global test_dfStrat
    global test_PSC
    global test_basement_cond
    
    test_conductivities = pd.Series([1.0,2.0,3.0])
    test_thicknesses = pd.Series([10.0,20.0,30.0])
    test_dfStrat = pd.DataFrame({'Conductivity':test_conductivities, 'Assumed':test_thicknesses})
    test_basement_depth = 60.
    test_basement_cond = 2.7
    test_well_depth = 40
    test_PSC = PSC(test_dfStrat, test_basement_depth, test_basement_cond, test_well_depth)
    
def teardown_function():
    """Teardown the matching constants from setup..."""
    global test_conductivities
    global test_thicknesses
    global test_basement_depth
    global test_well_depth
    global test_dfStrat
    global test_PSC
    global test_basement_cond
    
    del test_conductivities
    del test_thicknesses
    del test_basement_depth
    del test_well_depth
    del test_dfStrat
    del test_PSC
    del test_basement_cond
    
    
@with_setup(PSCKE_setup_function,teardown_function)
def TestBuiltWholeColumn():
    # We have correctly built the whole column
    assert((test_PSC.Whole_Sediment_Column_Cond == test_conductivities).all())
    assert((test_PSC.Whole_Sediment_Column_Thick == test_thicknesses).all())

    assert_equals(np.sum(test_PSC.Whole_Sediment_Column_Thick), 
                         np.sum(test_thicknesses))
    assert_equals(test_PSC.Kc, 2.0)
    
@with_setup(PSCKE_setup_function,teardown_function)
def TestBuiltWellColumn():
    # We have correctly built the well column
    assert((test_PSC.Well_Column_Cond == test_conductivities).all())
    test_thicknesses = np.array([10.,20.,10.])
    assert((test_PSC.Well_Column_Thick == test_thicknesses).all())

    assert_equals(np.sum(test_PSC.Well_Column_Thick), 
                         np.sum(test_thicknesses))
    assert_almost_equal(test_PSC.Kw, 1.7142857)
                         
@with_setup(PSCKE_setup_function,teardown_function)
def TestEstimateKForUpperPartialStratColumn():
    z = 35.
    k = test_PSC.EstimateKForUpperPartialStratColumn(z)
    assert_almost_equal(k,1.6153846)
    z = 50.
    k = test_PSC.EstimateKForUpperPartialStratColumn(z)
    assert_almost_equal(k,1.875)

@with_setup(PSCKE_setup_function,teardown_function)
def TestEstimateKForLowerPartialStratColumn():
    z = 50.
    Kw, Kss, Kpc = test_PSC.EstimateKForLowerPartialStratColumn(z)
    assert_almost_equal(Kw,1.7142857)
    assert_almost_equal(Kw,test_PSC.Kw)
    assert_almost_equal(Kpc,1.875)
    assert_almost_equal(Kss,3.0)
    
@raises(ValueError)
@with_setup(PSCKE_setup_function,teardown_function)
def TestEstimateKForLowerPartialStratColumnDepthLessThanWellDepth():
    #Depth is less than the well depth, so the error exception should be thrown.
    z = 30.
    Kw, Kss, Kpc = test_PSC.EstimateKForLowerPartialStratColumn(z)