import numpy as np

'''
This function is used to return the depth to a temperature of interest by linearly varying between estimated values.

1) Retrieve the temperature at or above the desired temperature. Return the depth.
2) Retrieve the temperature below the desired temperature. Return the depth.
3) Linearly solve for the depth of the desired temperature.

Fixme: This interpolation could be more exact by including the heat generation.
Using Taylor series expansion may also work

'''

def DepthToTemp(Temps, DesiredTemp, Depths):
            #Convert the list to an array for faster operation.
            Temps = np.array(Temps)

            #Check that the desired temperature is in Temps
            if DesiredTemp > Temps[len(Temps)-1]:
                #If not, assign a value of -9999, indicating that the temperature was not reached in Temps
                Depth_at_DesiredTemp = -9999.
            elif DesiredTemp < Temps[0]:
                Depth_at_DesiredTemp = -9999.
            else:
                #Determine the index of the closes temperature estimated to the desired temperature
                idx_closest = (np.abs(Temps - DesiredTemp)).argmin()
                if Temps[idx_closest] >= DesiredTemp:
                        #The next closest value must be located at the prior index.
                        idx_next = idx_closest - 1
                        #Linearly solve for the depth to the temperature
                        Depth_at_DesiredTemp = ((Depths[idx_closest] - Depths[idx_next])/ \
                                                (Temps[idx_closest] - Temps[idx_next]))* \
                                                (DesiredTemp - Temps[idx_next]) \
                                                + Depths[idx_next]
                else:
                        #The next closest value is located at the next index.
                        idx_next = idx_closest + 1
                        #Linearly solve for the depth to the temperature.
                        Depth_at_DesiredTemp = ((Depths[idx_next] - Depths[idx_closest])/ \
                                                (Temps[idx_next] - Temps[idx_closest]))* \
                                                (DesiredTemp - Temps[idx_closest]) \
                                                + Depths[idx_closest]
             
            return Depth_at_DesiredTemp
