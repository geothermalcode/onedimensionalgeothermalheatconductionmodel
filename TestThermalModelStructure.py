from nose import with_setup
from nose.tools import assert_equals, assert_raises, raises, assert_almost_equal
from CsvReader import read_wells, compute_gradients, read_strats
from TempEstimator import SedTempEst, BaseTempEst
from ThermalConductivityHarmonicAverager import HarmAvg, LowerLayerHarmAvg
from StratColumnIterator import StratColumnIterator
from PartialStratColKEst import PartialStratColumn as PSC
from DepthToTempIterator import DepthToTemp
from TempAtDepthIterator import TempAtDepth
import pandas as pd
import numpy as np

Wells = None
    
def ThermalModel_setup_function():
    """Setup a few constant thingies for testing ThermalModelStructure"""
    global Wells

    #Read in example well data from the ThermalModelStructure file
    Wells = read_wells(file='ExampleInputTable.csv',
                       skiprows=[],
                       dtypes={'RowID_':str})

def teardown_function():
    """Teardown the matching constants from setup..."""
    global Wells

    del Wells

@with_setup(ThermalModel_setup_function,teardown_function)
def TestRemoveNaNRowsAndColumns():
    global Wells

    correctRowLen = len(Wells)
    correctColLen = len(Wells.columns)
    
    #Delete nan rows from the read in data that may result from added rows at the end of the file:
    if np.isnan(Wells['WellDepth'][len(Wells)-1]):
        while np.isnan(Wells['WellDepth'][len(Wells)-1]):
            Wells = Wells.ix[:(len(Wells)-2)]
    #Delete NaN columns from the data.
    if all(np.isnan(Wells.ix[:,(len(Wells.columns)-1)])):
        while all(np.isnan(Wells.ix[:,(len(Wells.columns)-1)])):
            Wells = Wells.ix[:,:(len(Wells.columns)-1)]

    assert_equals(len(Wells), correctRowLen)
    assert_equals(len(Wells.columns), correctColLen)

@with_setup(ThermalModel_setup_function,teardown_function)
def TestAbAndQmantleConversion():
    global Wells

    correct_As = 1./1000000.
    correct_Qmantle = 30./1000.

    Wells['Qmantle'] /= 1000.
    Wells['SedRadHeat'] /= 1.e6

    assert_equals(correct_As, Wells['SedRadHeat'][1])
    assert_equals(correct_Qmantle, Wells['Qmantle'][1])

@with_setup(ThermalModel_setup_function,teardown_function)
def TestFlagWellDepthShallowerThanStartDepth():
    global Wells

    start_depth = 1020.
    match_test = np.where(np.array(Wells['WellDepth']) < start_depth)
    assert_equals(all(Wells['WellDepth'][np.array(match_test).tolist()[0]] < start_depth), True)

@with_setup(ThermalModel_setup_function,teardown_function)
def TestSurfaceHeatFlowValues():
    global Wells

    WellDepth = 1000.
    BasementDepth = 3000.
    BHT = 40.
    SurfTemp = 10.
    SedRadHeat = 1./1000000
    Kw = 2.2
    Kc = 2.5
    Qmantle = 30./1000
    basement_conductivity = 2.7
    B = 10000.
    
    #(WellDepth <= BasementDepth)
    Qs = ((BHT - SurfTemp)*Kw + \
          (SedRadHeat*(WellDepth)**2)/2.)/WellDepth

    assert_equals(Qs, 0.0665)

    WellDepth = 3500.
    BHT = 90.
    
    #Well > Basement
    Qs = ((BHT - SurfTemp) + \
          ((SedRadHeat*(BasementDepth)**2)/(2.*Kc)) + \
          (Qmantle/((1. - np.exp(-3.))*basement_conductivity))*(B*(1.-np.exp(-(WellDepth-BasementDepth)/B)) - (WellDepth-BasementDepth)) + \
          ((SedRadHeat*BasementDepth)/basement_conductivity)*((WellDepth-BasementDepth)*(1. - 1./(1. - np.exp(-3.))) + B*(1. - np.exp(-(WellDepth - BasementDepth)/B))/(1. - np.exp(-3.)))) / \
          (BasementDepth/Kc + (WellDepth-BasementDepth)/basement_conductivity * (1. - 1./(1. - np.exp(-3.))) + \
           B*(1. - np.exp(-(WellDepth-BasementDepth)/B))/(basement_conductivity * (1. - np.exp(-3.))))

    assert_almost_equal(Qs, 0.0595463818)

@with_setup(ThermalModel_setup_function,teardown_function)
def TestDepthVectorAssignment():
    global Wells

    #Well shallower than MaxDepth
    WellDepth = 2000.
    MaxDepth = 3000.
    depth_increment = 10.
    start_depth = 10.
    Depths = np.arange(start_depth,
                               WellDepth+0.00001,  # keep WellDepth if integer multiple of increment.
                               depth_increment)

    assert_equals(Depths[0], start_depth)
    assert_equals(Depths[-1], WellDepth)
    assert_equals(len(Depths), ((WellDepth-start_depth)/start_depth)+1)

    #Well deeper than MaxDepth

    MaxDepth = 1500.
    
    Depths = np.arange(start_depth,
                               MaxDepth+0.00001,  # keep MaxDepth if integer multiple of increment.
                               depth_increment)

    assert_equals(Depths[0], start_depth)
    assert_equals(Depths[-1], MaxDepth)
    assert_equals(len(Depths), ((MaxDepth-start_depth)/start_depth)+1)

    def DepthVectorCheck(Depths,Depths2,Depths3):
        if Depths3 == None:
            if Depths2 == None:
                all_depths = Depths
            else:
                all_depths = np.concatenate((Depths,Depths2))
        else:
            if Depths2 == None:
                all_depths = np.concatenate((Depths,Depths3))
            else:
                all_depths = np.concatenate((Depths,Depths2,Depths3))
        return(all_depths)

    Depths = [10.,20.,30.,40.]
    Depths2 = [50.,60.,70.]
    Depths3 = [80.,90.,100.]
    allDepths = DepthVectorCheck(Depths,Depths2,Depths3)
    assert_equals(len(allDepths), 10)

    Depths = [10.,20.,30.,40.,50.,60.,70.]
    Depths2 = None
    Depths3 = [80.,90.,100.]
    allDepths = DepthVectorCheck(Depths,Depths2,Depths3)
    assert_equals(len(allDepths), 10)
    
    Depths = [10.,20.,30.,40.,50.,60.,70.]
    Depths2 = [80.,90.,100.]
    Depths3 = None
    allDepths = DepthVectorCheck(Depths,Depths2,Depths3)
    assert_equals(len(allDepths), 10)

    Depths = [10.,20.,30.,40.,50.,60.,70.,80.,90.,100.]
    Depths2 = None
    Depths3 = None
    allDepths = DepthVectorCheck(Depths,Depths2,Depths3)
    assert_equals(len(allDepths), 10)
    
if __name__ == '__main__':
    import nose
    nose.run(argv=[__file__, '--with-doctest', '-v'\
                  ])
