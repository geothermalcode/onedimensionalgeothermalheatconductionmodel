from nose import with_setup
from nose.tools import assert_equals, assert_raises, raises, assert_almost_equal
import pandas as pd
from ThermalConductivityHarmonicAverager import HarmAvg as HA
from ThermalConductivityHarmonicAverager import LowerLayerHarmAvg as LLHA

test_conductivities = None
test_bad_conductivities = None
test_thicknesses = None
correct_answer = None
test_partial_column_depth = None
test_basal_conductivities = None
test_DOI = None
test_well_depth = None
test_Kw = None

def HA_setup_function():
    """Setup a few constant thingies for testing the HarmonicAverager"""
    global test_conductivities
    global test_bad_conductivities
    global test_thicknesses
    global correct_answer
    global test_basal_conductivities
    global test_partial_column_depth
    global test_DOI
    global test_well_depth
    global test_Kw
    
    test_conductivities = pd.Series([1.0,2.0,3.0])
    test_bad_conductivities = pd.Series([1.0,2.0,0.0])
    test_thicknesses = pd.Series([10.0,20.0,30.0])
    # The harm avg with good conductivities should be (10+20+30) [m] / (10/1 + 20/2 + 30/3) [m^2K/W] = 60 [m]/30 [m^2K/W] = 2 [W/mK]
    correct_answer = 2.0
    test_DOI = 60.
    test_basal_conductivities = None
    test_partial_column_depth = None
    test_well_depth = None
    test_Kw = None
    
def LLHA_setup_function():
    """Setup a few constant thingies for testing LowerLayerHarmonicAverager"""
    global test_conductivities
    global test_basal_conductivities
    global test_thicknesses
    global test_partial_column_depth
    global test_bad_conductivities
    global correct_answer
    global test_DOI
    global test_well_depth
    global test_Kw
    
    test_conductivities = pd.Series([1.0,2.0,3.0])
    test_thicknesses = pd.Series([10.0,20.0,30.0])
    test_partial_column_depth = 60.
    test_basal_conductivities = 2.
    test_well_depth = 40.
    test_Kw = 1.7142857
    test_bad_conductivities = None
    correct_answer = None
    test_DOI = None

def teardown_function():
    """Teardown the matching constants from setup..."""
    global test_conductivities
    global test_bad_conductivities
    global test_thicknesses
    global correct_answer
    global test_basal_conductivities
    global test_partial_column_depth
    global test_DOI
    global test_well_depth
    global test_Kw
    
    del test_conductivities
    del test_bad_conductivities
    del test_thicknesses
    del correct_answer
    del test_basal_conductivities
    del test_partial_column_depth
    del test_DOI
    del test_well_depth
    del test_Kw
    
@with_setup(HA_setup_function,teardown_function)
def testHarmonicAverage():
    #Test using 60m as the DOI.
    answer = HA(test_conductivities,test_thicknesses,test_DOI)
    assert_equals(answer,correct_answer)
 
@with_setup(HA_setup_function,teardown_function)
def testHarmonicAverageDOILessThanColumnThickness():
    test_DOI = 50.
    
    answer = HA(test_conductivities,test_thicknesses,test_DOI)
    assert_equals(answer,1.875)
    
@with_setup(HA_setup_function,teardown_function)
def testHarmonicAverageDOIOnBoundary():
    test_DOI = 30.
    
    answer = HA(test_conductivities,test_thicknesses,test_DOI)
    assert_equals(answer,1.5)


@raises(ZeroDivisionError)
@with_setup(HA_setup_function,teardown_function)
def testHarmonicAverageFailsWithZeroDivide():
    global test_bad_conductivities
    global test_thicknesses
    HA(test_bad_conductivities,test_thicknesses,test_DOI)

@with_setup(LLHA_setup_function,teardown_function)
def testLowerLayerHarmonicAverageConductivityAveraging():
    global test_conductivities
    global test_thicknesses
    global test_basal_conductivities
    global test_partial_column_depth
    global test_well_depth
    global test_Kw
    
    #Test for when no Kw is specified. This should return 1.714 for Kw.
    #Kc should be equal to the full column average conductivity of 2.0
    Kw,Kss,Kc = LLHA(test_conductivities,
                     test_thicknesses,
                     test_partial_column_depth,
                     test_well_depth,
                     test_basal_conductivities)
            
    assert_almost_equal(Kc, 2.0)
    assert_almost_equal(Kw, 1.7142857)
    assert_almost_equal(Kss,test_conductivities.values[-1])
    
@with_setup(LLHA_setup_function,teardown_function)
def testLowerLayerHarmonicAverageWithKwSpecified():
    global test_conductivities
    global test_thicknesses
    global test_basal_conductivities
    global test_partial_column_depth
    global test_well_depth
    global test_Kw
    
    #Test for when no Kw is specified. This should return 1.714 for Kw.
    #Kc should be equal to the full column average conductivity of 2.0
    Kw,Kss,Kc = LLHA(test_conductivities,
                     test_thicknesses,
                     test_partial_column_depth,
                     test_well_depth,
                     test_basal_conductivities,
                     Kw = test_Kw)
            
    assert_almost_equal(Kc, 2.0)
    assert_almost_equal(Kw, 1.7142857)
    assert_almost_equal(Kss,test_conductivities.values[-1], 6)
    
@with_setup(LLHA_setup_function,teardown_function)
def testLowerLayerHarmonicAveragePartialColDepthLessThanColumnDepth():
    global test_conductivities
    global test_thicknesses
    global test_basal_conductivities
    global test_partial_column_depth
    global test_well_depth
    global test_Kw
    
    test_partial_column_depth = 50.
    
    #Test for when no Kw is specified. This should return 1.714 for Kw.
    Kw,Kss,Kc = LLHA(test_conductivities,
                     test_thicknesses,
                     test_partial_column_depth,
                     test_well_depth,
                     test_basal_conductivities)
            
    assert_almost_equal(Kc, 1.875)
    assert_almost_equal(Kw, 1.7142857)
    assert_almost_equal(Kss,test_conductivities.values[-1])

@raises(ValueError)    
@with_setup(LLHA_setup_function,teardown_function)
def testLowerLayerHarmonicAveragePartialColDepthLessThanWellDepth():
    global test_conductivities
    global test_thicknesses
    global test_basal_conductivities
    global test_partial_column_depth
    global test_well_depth
    global test_Kw
    
    test_partial_column_depth = 20.
    
    Kw,Kss,Kc = LLHA(test_conductivities,
                     test_thicknesses,
                     test_partial_column_depth,
                     test_well_depth,
                     test_basal_conductivities)

@raises(ValueError)    
@with_setup(LLHA_setup_function,teardown_function)
def testLowerLayerHarmonicAveragePartialColDepthEqualsWellDepth():
    global test_conductivities
    global test_thicknesses
    global test_basal_conductivities
    global test_partial_column_depth
    global test_well_depth
    global test_Kw
    
    test_partial_column_depth = test_well_depth
    
    Kw,Kss,Kc = LLHA(test_conductivities,
                     test_thicknesses,
                     test_partial_column_depth,
                     test_well_depth,
                     test_basal_conductivities)

@with_setup(LLHA_setup_function,teardown_function)
def testLowerLayerHarmonicAveragePartialColDepthOnLayerBoundary():
    global test_conductivities
    global test_thicknesses
    global test_basal_conductivities
    global test_partial_column_depth
    global test_well_depth
    global test_Kw
    
    test_well_depth = 20.
    test_partial_column_depth = 30.
    
    Kw,Kss,Kc = LLHA(test_conductivities,
                     test_thicknesses,
                     test_partial_column_depth,
                     test_well_depth,
                     test_basal_conductivities)
            
    assert_almost_equal(Kc, 1.5)
    assert_almost_equal(Kw, 1.333333333)
    assert_almost_equal(Kss,test_conductivities.values[-2])

                     
if __name__ == '__main__':
    import nose
    nose.run(argv=[__file__, '--with-doctest', '-v'])
