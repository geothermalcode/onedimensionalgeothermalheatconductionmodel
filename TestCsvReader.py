from nose import with_setup
from nose.tools import assert_equals, assert_almost_equal
import numpy as np
import CsvReader as CR

cnst = {}

def WC_setup_function():
    """Setup a few constant thingies for testing WellsCloud.csv"""
    global cnst
    cnst['cols'] = 7 + 1 # Adding one for Gradient Estimate
    cnst['rows'] = 1028
    cnst['header'] = ['API','BHT','WellDepth','Qmantle','BasementDepth','SurfTemp','SedRadHeat'] +\
                     ['Gradient']
    #cnst['row_1'] = ['dimless', 'Celsius', 'm', 'mW/m^2', 'm', 'Celsius', 'uW/m^3']
    cnst['first_data'] = [37039200640000,62.2,2115.62,30,2743,9,1.00] +\
                         [25.14629281]
    # N.B. Through happenstance, these strings are also passing due to the casts 
    # to np.float64 in the compareSequences function...
    cnst['last_row'] = ['37123435110000','53.2','1742.2368','30','3289','9','1.00'] +\
                       ['25.36968568']
    cnst['wellfile']='WellsCloud.csv'
    
def teardown_function():
    """Teardown the matching constants from setup..."""
    global cnst
    # This ensures that the cnst dict has not been stomped on by something in the tests
    # and is an empty dict which will contain only what is set in setup
    # for the next test
    cnst={}
    
def compareSequences(first,second):
    for known,read in zip(first,second):
        try:
            assert_equals(known,read)
        except AssertionError:
            assert_almost_equal(np.float64(known),np.float64(read))

@with_setup(WC_setup_function,teardown_function)
def testWellsCloudShape():
    df = CR.read_wells(file=cnst['wellfile'],skiprows=[1])
    CR.compute_gradients(df)
    # Check that we got the correct shape...
    rows,cols = df.shape
    assert_equals(cnst['cols'],cols)
    assert_equals(cnst['rows'],rows)

@with_setup(WC_setup_function,teardown_function)
def testWellsCloudContents():
    df = CR.read_wells(file=cnst['wellfile'], skiprows=[1])
    CR.compute_gradients(df)
    
    # Test that we read in the header ok
    compareSequences(cnst['header'],df.columns)
    # Test that we read in the units row ok
    #compareSequences(cnst['row_1'],df.values[0,:])
    # test that we read in the first data row ok
    compareSequences(cnst['first_data'],df.values[0,:])
    # test that we read in the last data row ok
    compareSequences(cnst['last_row'],df.values[-1,:])

@with_setup(WC_setup_function,teardown_function)
def testWellsCloudContentsWithDtypes():
    df = CR.read_wells(file=cnst['wellfile'], skiprows=[1],
                       dtypes={'API':str})
    CR.compute_gradients(df)
    
    # Test that we read in the header ok
    compareSequences(cnst['header'],df.columns)
    # Test that we read in the units row ok
    #compareSequences(cnst['row_1'],df.values[0,:])
    # test that we read in the first data row ok
    compareSequences(cnst['first_data'],df.values[0,:])
    # test that we read in the last data row ok
    compareSequences(cnst['last_row'],df.values[-1,:])

@with_setup(WC_setup_function,teardown_function)
def testWellsCloudGradients():
    df = CR.read_wells(file=cnst['wellfile'], skiprows=[1])
    CR.compute_gradients(df)
    compareSequences(df['Gradient'],1000.*(df['BHT']-df['SurfTemp'])/df['WellDepth'])
    
    
def SC_setup_function():
    """Setup a few constant thingies for testing StratCol21.csv"""
    global cnst
    cnst['cols'] = 5
    cnst['rows'] = 42
    cnst['header'] = ['ID','Min','Max','Assumed','Conductivity']
    # N.B. These numerical values were read in as numerics instead of strings
    # That's because all rows contained the same types (presumably)
    cnst['first_data'] = ['Glenshaw',0,114,57,3.80]
    cnst['last_row'] = ['Potsdam',16,32,24,3.70]
    cnst['stratfile']='StratCol21.csv'
    

@with_setup(SC_setup_function,teardown_function)
def testStratColsShape():
    df = CR.read_wells(file=cnst['stratfile'])
    rows,cols = df.shape
    assert_equals(cnst['cols'],cols)
    assert_equals(cnst['rows'],rows)

@with_setup(SC_setup_function,teardown_function)
def testStratColsContents():
    df = CR.read_wells(file=cnst['stratfile'])
    # Test that we read in the header ok
    compareSequences(cnst['header'],df.columns)
    # Test that we read in the first data row ok...
    compareSequences(cnst['first_data'],df.values[0,:])
    # Test that we read in the last data row ok...
    compareSequences(cnst['last_row'],df.values[-1,:])

@with_setup(SC_setup_function,teardown_function)
def testStratColsAssumed():
    df = CR.read_wells(file=cnst['stratfile'])
    assert_equals(df.columns[1],'Min')
    assert_equals(df.columns[2],'Max')
    assert_equals(df.columns[3],'Assumed')
    assert_equals((df['Min'] <= df['Assumed']).all(),True)
    assert_equals((df['Max'] >= df['Assumed']).all(),True)
    assert_equals((df['Conductivity'] != 0.).all(),True)

def ReadStrats_setup_function():
    """Setup a few constant thingies for testing ReadStrats"""
    global Keys
    global Files
    global Data

    Keys = [2701, 2801, 2901, 3001, 3101]
    Files = ['NY27.csv', 'NY28.csv', 'NY29.csv', 'NY30.csv', 'NY31.csv']
    Data = {}

def ReadStrats_teardown_function():
    """Teardown the matching constants from ReadStratssetup..."""
    global Keys
    global Files
    global Data

    del Keys
    del Files
    del Data

@with_setup(ReadStrats_setup_function,ReadStrats_teardown_function)
def testLoadingStratKeys():
    global Keys
    global Files
    global Data
    
    FieldName = 'Carter Conductivity'

    Cols = CR.read_strats(Data, Keys, Files, FieldName)    
    assert_equals(len(Cols), len(Files))
    assert_equals(len(Cols[Keys[0]]), len(Data[Keys[0]]))
    assert_equals(len(Cols[Keys[0]].columns), len(Data[Keys[0]].columns))

@with_setup(ReadStrats_setup_function,ReadStrats_teardown_function)
def testNaNRowsAndColumnsRemoved():
    global Keys
    global Files
    global Data
    
    FieldName = 'Carter Conductivity'

    Cols = CR.read_strats(Data, Keys, Files, FieldName)    
    assert_equals(len(Cols[Keys[0]]), 26)
    assert_equals(len(Cols[Keys[0]].columns), 16)

if __name__ == '__main__':
    import nose
    nose.run(argv=[__file__, '--with-doctest', '-v'\
                  ])
