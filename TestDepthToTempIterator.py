from nose import with_setup
from nose.tools import assert_equals, assert_raises, raises, assert_almost_equal
import pandas as pd
import numpy as np
from DepthToTempIterator import DepthToTemp
from math import exp

Temps = None
Depths = None
DesiredTemp = None
                        
def DepthToTemp_setup_function():
    """Setup a few constant thingies for testing DepthToTempIterator"""
    global Temps
    global Depths
    global DesiredTemp
    
    Temps = [60., 70., 80., 90., 100.]  # C
    Depths = [1000., 1300., 1800., 1900., 2000.] # m
    DesiredTemp = 84.
     
def teardown_function():
    """Teardown the matching constants from setup..."""
    global Temps
    global Depths
    global DesiredTemp

    del Temps
    del Depths
    del DesiredTemp
    
    
@with_setup(DepthToTemp_setup_function,teardown_function)
def testConvertTempsToArray():
    global Temps
    global Depths
    global DesiredTemp
    
    TempsArray = np.array(Temps)
    assert_equals(TempsArray[0], Temps[0])

@with_setup(DepthToTemp_setup_function,teardown_function)
def testMinus9999ForTempOutOfRange():
    global Temps
    global Depths
    global DesiredTemp

    DesiredTemp = 110
    answer = DepthToTemp(Temps, DesiredTemp, Depths)
    assert_equals(-9999.,answer)
    
    DesiredTemp = 50
    answer = DepthToTemp(Temps, DesiredTemp, Depths)
    assert_equals(-9999.,answer)
    
@with_setup(DepthToTemp_setup_function,teardown_function)
def testIndexGreaterThanDesiredTemp():
    global Temps
    global Depths
    global DesiredTemp

    DesiredTemp = 86.

    answer = DepthToTemp(Temps, DesiredTemp, Depths)
    correct_answer = 1860.
    assert_equals(correct_answer,answer)

@with_setup(DepthToTemp_setup_function,teardown_function)
def testIndexLessThanDesiredTemp():
    global Temps
    global Depths
    global DesiredTemp

    answer = DepthToTemp(Temps, DesiredTemp, Depths)
    correct_answer = 1840.
    assert_equals(correct_answer,answer)

@with_setup(DepthToTemp_setup_function,teardown_function)
def testFirstIndexIsClosest():
    global Temps
    global Depths
    global DesiredTemp

    DesiredTemp = 60.
    answer = DepthToTemp(Temps, DesiredTemp, Depths)
    correct_answer = 1000.
    assert_equals(correct_answer,answer)

    DesiredTemp = 64.
    answer = DepthToTemp(Temps, DesiredTemp, Depths)
    correct_answer = 1120.
    assert_equals(correct_answer,answer)

@with_setup(DepthToTemp_setup_function,teardown_function)
def testLastIndexIsClosest():
    global Temps
    global Depths
    global DesiredTemp

    DesiredTemp = 100.
    answer = DepthToTemp(Temps, DesiredTemp, Depths)
    correct_answer = 2000.
    assert_equals(correct_answer,answer)

    DesiredTemp = 97.
    answer = DepthToTemp(Temps, DesiredTemp, Depths)
    correct_answer = 1970.
    assert_equals(correct_answer,answer)
   
if __name__ == '__main__':
    import nose
    nose.run(argv=[__file__, '--with-doctest', '-v'])
