from nose import with_setup
from nose.tools import assert_equals, assert_raises, raises, assert_almost_equal
import pandas as pd
from TempEstimator import SedTempEst as SedTerm
from TempEstimator import BaseTempEst as BaseTerm
from math import exp
import numpy as np

test_K = None
test_Z = None
test_Q = None
test_A = None
test_B = None
Basement_deltaT = None

                        
def SedTerm_setup_function():
    """Setup a few constant thingies for testing TempEstimator"""
    global test_K
    global test_Z
    global test_Q
    global test_A
    global test_B
    global correct_answer
    global Basement_deltaT
    
    test_K = 2.0               # W/m-K
    test_Z = 1000.0            # m
    test_Q = 50.0e-3           # W/m^2
    test_A = 1.0e-6            # W/m^3
    test_B = 10000             # m

    Basement_deltaT = None
    
   # simple_well_depth_deltaT = ((test_Q*test_Z)/(test_K) - 
   #                             (test_A*(test_Z**2))/(2.*test_K))
   # Correct answer for those numbers is 24.75 K
    correct_answer = 24.75
     
def teardown_function():
    """Teardown the matching constants from setup..."""
    global test_K
    global test_Z
    global test_Q
    global test_A
    global test_B
    global correct_answer
    global Basement_deltaT

    del test_K
    del test_Z
    del test_Q
    del test_A
    del test_B
    del correct_answer
    del Basement_deltaT
    
    
@with_setup(SedTerm_setup_function,teardown_function)
def testSedTerm():
    global test_K
    global test_Z
    global test_Q
    global test_A
    global test_B
    global correct_answer
    global Basement_deltaT
    
    answer = SedTerm(test_K, test_Z, test_Q, test_A)
    assert_equals(answer,correct_answer)
    

def BaseTerm_setup_function():
   """Setup a few constant thingies for testing TempEstimator"""
   global test_K
   global test_Z
   global test_Q
   global test_A
   global test_B
   global correct_answer
   global Basement_deltaT

   test_K = 2.0               # W/m-K
   test_Z = 1000.0            # m
   test_Q = 30.0e-3           # W/m^2
   test_A = 3.0e-6            # W/m^3
   test_B = 10000             # m

   Basement_deltaT = ((test_Q*test_Z - test_A*test_B*test_Z*np.exp(-3.))/(test_K) +
                      (test_A*(test_B**2)*(1. - exp(-(test_Z/test_B))))/test_K)
                      
   # Correct answer for those numbers is 28.527 K
   correct_answer = 28.52758127
   
   
@with_setup(BaseTerm_setup_function,teardown_function)
def testBaseTerm():
    global test_K
    global test_Z
    global test_Q
    global test_A
    global test_B
    global correct_answer
    global Basement_deltaT
    
    assert_almost_equal(Basement_deltaT,correct_answer,places=4)
    answer = BaseTerm(test_K, test_Z, test_Q, test_A, test_B)
    assert_almost_equal(answer, correct_answer)
   
if __name__ == '__main__':
    import nose
    nose.run(argv=[__file__, '--with-doctest', '-v'])
