This model is used to calculate geotherms for wells with Bottomhole Temperature (BHT) measurements using a 1-D, steady state heat conduction method. 
Rock thermal conductivity stratigraphy must be known for each well. Here, generalized AAPG (1985) COSUNA columns are used, but specific stratigraphy for each well may easily be added.
There is an assumed heat generation of 1 uW/m^3 in sedimentary rocks, and an exponential decay of heat generation in the basement rocks, as per Lachenbruch (1968). 
The surface heat flow is solved for analytically, accounting for the two heat generation layers. As a result, the BHTs that are input to this model are reproduced at their measurement depth.

The ThermalModelStructure.py file is the main file that calls functions contained within all of the other .py files. 
The main output is the surface heat flow, and the geotherm temperature profile between the specified depths of interest.
Alternatively to using BHTs as the "known" thermal variable, one may instead use the surface heat flow. The ThermalModelStructureFromHeatFlow.py script is used for this purpose.
A Microsoft Word document within this repository contains a readme for potential tweaks to the model for different sets of assumptions.

The CsvReader.py file contains functions that 1) read in the well database, 2) compute the thermal gradient, and 3) read in the stratigraphy files.
The StratColumnIterator.py file has functions that compile the stratigraphic column, and scale the column to the basement depth at the location of each well.
The ThermalConductivityHarmonicAverager.py file has functions that compute the thermal conductivity at a given depth in the strat column or the basement.
The PartialStratColumnKEst.py contains functions that estimate the harmonic average thermal conductivity for some specified portion of the column.
The TempEstimator.py file has two functions that are used to calculate the temperature at depth in 1) sedimentary rocks, and 2) basement rocks.
The TempAtDepthIterator.py file has a function that linerly interpolates between calculated temperatures at depth in order to find the temperature at a depth of interest.
The DepthToTempIterator.py file has a function that linerly interpolates between calculated temperatures at depth in order to find the depth to a temperature of interest.

Documentation of the assumptions, methods, and general equations used in this thermal model are contained within the following documents:
Smith, J.D. and F.G. Horowitz. “Well database organization and thermal model methods” in Low Temperature Geothermal Play Fairway Analysis for the Appalachian Basin: Final Phase 1 Research Report, U.S. Dept. of Energy Award No. DE-EE0006726. Principal Investigator Teresa Jordan. Submitted Oct. 16, 2015.