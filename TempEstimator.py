from math import exp
import numpy as np

def SedTempEst(Cond, Depth, HeatFlux, RadioSed):
    """Base routine that computes a term in the sedimentary
       column \Delta T ODE integration.
       
       HeatFlux has units of W/m**2
       Depth has units of m
       Cond has units of W/m-K
       RadioSed has units of W/m**3
       (MKS! Be careful that you pass in the correct orders of magnitudes)
       
        Different terms in the ODE integration correspond to different
        arguments to this function. They must be taken care of externally to
        this routine.
        
        NOTES ON INPUT VARIABLES: 
        When estimating in sedimentary rocks below the well, Depth=(DOI-well_depth)
        When estimating in sedimentary rocks below the well, HeatFlux=(Qs-RadioSed*well_depth)
        Maximum value of the Depth is the sediment thickness.
        Conductivity is the average conductivity to the DOI
        
    """
    return (HeatFlux*Depth/Cond) - (RadioSed*((Depth)**2))/(2*Cond)
    
def BaseTempEst(Cond, Depth, HeatFlux, RadioBase, B):
    
    """This routine implements the exponential decay in the basement to the
       depth of 3B in the basement rocks.
       
       HeatFlux has units of W/m**2. This is the assumed mantle heat flux
        at the depth 3B.
       Depth has units of m. This depth is the (DOI - BasementDepth)
       Cond has units of W/m-K. This is the basement conductivity. 
       RadioBase has units of W/m**3. This is the heat generation at the top
        of the basement. This is computed in ThermalModelStructure function.
       B has units of m
       (MKS! Be careful that you pass in the correct orders of magnitudes)
    
       This is the only term in the ODE integration that utilizes this basement 
       temperature estimation.
    """
    
    return (HeatFlux - RadioBase*B*np.exp(-3.))*Depth/Cond + (RadioBase*(B**2)*(1. - np.exp(-(Depth/B))))/Cond
