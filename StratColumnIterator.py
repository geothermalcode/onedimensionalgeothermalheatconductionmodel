"""This generator yields iterates of (thick,cond) through a strat column.
   It deals with the special cases of being partially in the interior of a 
   stratigraphic layer, and with being in the basement below the bottom of 
   a strat column.
   
   The thickness of each layer is scaled by the total sediment thickness 
   (basement depth) at the well location 
   compared to the thickness of the COSUNA column.
   
   Its intended usage is:
   
   for thick,cond in StratColumnIterator(StratFrame,doi,[sediment_thick],[bsmnt_cond]):
       {do something with thick,cond, 
       like appending them to lists that will be passed to the Harmonic Averager
       the ODE integration evaluator, etc.}
   {call_the_ODE_evaluator(thick_list,cond_list)}
"""

import numpy as np


def StratColumnIterator(StratFrame, depth_of_interest, WellSedThick=None, basement_cond=None):
    #Initialize the thickness of the column to 0.
    tot_thick = 0.0
    
    #Calculate the total COSUNA column thickness. This is used for stretching/scaling.
    ColSedThick = np.sum(StratFrame['Assumed'])
    
    if WellSedThick == None:
        raise ValueError("Specify the sedimentary thickness (basement depth) so that the column can be stretched")
    for thick,cond in zip(StratFrame['Assumed'], StratFrame['Conductivity']):
        # Account for thickness scaling demanded by (e.g.) using the COSUNA columns' stratigraphy
		#FIXME: This linearly scales all layers, without regard to the maximum and minimum possible thickness of the layer.
		# A large loop is needed to check for violations and scale the rest of the layers accordingly.
		# This could lead to issues with a WellSedThick being too small or too large for layers to be scaled. 
       # An if statement checking for WellSedThick > sum(Strat['Min']) or WellSedThick < sum(Strat['Min']) could be added, 
       # but there would still be a problem of preferential scaling of some layers over others.
        thick *= WellSedThick/ColSedThick
        
        if (tot_thick + thick) <= depth_of_interest:
            # Still in a whole layer of the strat column
            tot_thick += thick
            yield thick, cond
            if np.allclose(tot_thick,depth_of_interest) and (basement_cond == None):
                # At the botttom of the strat column with no basement_cond
                return
        else:
            # Yielding the increment into the last layer, and its conductivity
            # still within the strat column
            yield (depth_of_interest - tot_thick), cond
            # Done because caller doesn't want anything below here
            return
    
    # CORNER CASE!
    # Exactly at the bottom of the strat col, within roundoff of the
    # basement. Don't return the conductivity with a ~zero thickness, 
    # instead stop the iteration.
    if np.allclose(depth_of_interest-tot_thick,0.0):
        return
    
    # We've iterated through the whole strat column but still have an interval
    # in the basement
    if basement_cond != None:
        yield (depth_of_interest-tot_thick),basement_cond
    else:
        raise ValueError("Need a basement conductivity specified.")