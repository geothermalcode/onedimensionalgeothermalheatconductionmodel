import pandas as pd
import numpy as np

"""Take thermal conductivity values and layer thicknesses and compute
a Harmonic Mean for them.

The sequences of values (e.g. conductivities) and thicknesses are intended 
to be constructed by the output of the StratColumnIterator (hence will already 
have been scaled to the COSUNA thicknesses) or will be input directly from 
a known sedimentary column for the well. That allows this code to simply 
calculate with what has been handed to it.
"""

def HarmAvg(values,thicknesses,z):
    # Evaluate the Harmoic average to the depth of interest (z) in a column.
    # Set the resistance and total thicknesss to 0.
    resist = 0.0
    total_thick = 0.0
    for cond,thick in zip(values, thicknesses):
        if cond == 0.0:
            raise(ZeroDivisionError)
        if (total_thick + thick) > z:
            # Deeper than the depth of interest in the column
            # Add the incremental resistance, 
            resist += (z - total_thick)/cond
            # Truncate total_thick to z, and 
            total_thick = z
            # get out of the loop
            break
        resist += thick/cond
        total_thick += thick
    
    return total_thick/resist
    
""" 
The LowerLayerHarmAvg function is intended to evaluate the harmonic conductivity below
the well and above some depth of interest (DOI) in the sediments via the following 
algebraic identity.

Down --->
surface     Well Bottom        DOI
|<---- Kw ----->|<---- Kss ---->|
|<------------ Kpc ------------>|


Define the harmonic conductivity of a composite of two layers 
Kw and Kss with thicknesses Zw and Zss to be equal to Kpc with thickness Zpc. 
By the harmonic averaging procedure the algebraic relation between them all is:

Zpc*Kpc**-1 = Zw*Kw**-1 + Zss*Kss**-1

A little algebraic re-arrangement leads to:
Zss*Kss**-1 = Zpc*Kpc**-1 - Zw*Kw**-1

Hence, if we calculate Kw and Kpc in the usual fashion using the HarmAvg(), 
we can calculate Kss from the above relationship.

This routine implements that method, returning a tuple of Kw, Kss, and Kpc
"""

def LowerLayerHarmAvg(all_values, 
                      all_thicknesses, 
                      partial_column_depth,
                      well_depth,
                      basal_conductivity, #FIXME! Is this needed? We do not call it anywhere, and this function is for the sediment section only.
                      Kw = None):
    # all_values and all_thicknesses must contain the scaled thicknesses
    # to the bottom of the sedimentary column.
    
    if partial_column_depth <= well_depth:
        raise ValueError("The partial column depth must be greater than the well depth.")
    
    if Kw == None:
        Kw = HarmAvg(all_values, all_thicknesses, well_depth)
    
    Kpc = HarmAvg(all_values, all_thicknesses, partial_column_depth)
    
    inv_Kss = (partial_column_depth*(1./Kpc) - well_depth*(1./Kw))/(partial_column_depth - well_depth)
    
    Kss = 1./inv_Kss
    
    return Kw, Kss, Kpc
