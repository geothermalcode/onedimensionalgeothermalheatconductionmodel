import pandas as pd
import numpy as np

def read_wells(file=None,skiprows=[],dtypes={}):
    df = pd.read_csv(file,skiprows=skiprows,dtype=dtypes)
    return df
    
def compute_gradients(df):
    #Assumes that the well depth is in units of meters, hence the 1000. constant.
    df['Gradient'] = 1000.*(df['BHT'] - df['SurfTemp'])/df['WellDepth']

def read_strats(Data, Keys, Filenames, ConductivityFieldName):
    #Function for efficiently reading in strat files and cleaning them up.
    for k, f, in zip(Keys, Filenames):
        Data[k] = read_wells(file = f, dtypes={'ColumnMin':np.float64,
                                               'Max': np.float64,
                                               'Assumed': np.float64,
                                               ConductivityFieldName: np.float64})
        #Check for and remove nan columns.
	if isinstance(Data[k].ix[0,(len(Data[k].columns)-1)], float):
                if np.isnan(Data[k].ix[0,(len(Data[k].columns)-1)]):
                        while np.isnan(Data[k].ix[0,(len(Data[k].columns)-1)]):
                                Data[k] = Data[k].drop(Data[k].columns[[(len(Data[k].columns)-1)]], axis=1)
                                if isinstance(Data[k].ix[0,(len(Data[k].columns)-1)], float):
                                        continue
                                else:
                                        break
	Data[k]['Conductivity'] = Data[k][ConductivityFieldName]
        #Check for and remove nan rows.
        if np.isnan(Data[k]['Conductivity'][(len(Data[k]['Conductivity'])-1)]):
                while np.isnan(Data[k]['Conductivity'][(len(Data[k]['Conductivity'])-1)]):
                        Data[k] = Data[k][:(len(Data[k]['Conductivity'])-1)]
    return Data
