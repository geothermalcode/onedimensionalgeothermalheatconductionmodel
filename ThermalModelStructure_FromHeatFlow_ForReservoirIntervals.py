from CsvReader import read_wells, compute_gradients, read_strats
from TempEstimator import SedTempEst, BaseTempEst
from ThermalConductivityHarmonicAverager import HarmAvg, LowerLayerHarmAvg
from StratColumnIterator import StratColumnIterator
from PartialStratColKEst import PartialStratColumn as PSC
from DepthToTempIterator import DepthToTemp
from TempAtDepthIterator import TempAtDepth
import pandas as pd
import numpy as np
import matplotlib as mpl
mpl.use('Agg')
from matplotlib import pyplot as plt
import math

"""
This is the main file that calls functions that calculate the temperature
at depth. The model assumes that there are layer(s) of sediment above
basement rocks (crust). The radiogenic heat production (RHP) in the basement is not known, 
and is approximated by an exponential decay in RHP that begins 
at the surface of the basement and ends at the Moho. Knowledge of the RHP in the 
sediment is assumed to be known/approximated by, for instance, gamma ray logs.    

Assumes that the Wells input file has at least these 7 columns:
Qmantle [mW/m**2]  - heat flow at the Moho
SedRadHeat [uW/m**3] - RHP in sediments
WellDepth [m] - depth of BHT measurement
SurfTemp [C] - average annual surface temperature
BHT [C] - (corrected appropriately)
RowID_ [] - Unique well identifier
ROME_ID [] - to identify if a well is in the Rome Trough
COSUNA_ID [] - to identify the strat column


Assumes that the Strat input file contains at least these 2 columns
Assumed [m] - thickness of the formation 
Conductivity [W/mK] - thermal conductivity of the formation
FIXME: For a layer-by-layer model, we would also want to include 
the radiogenic heat production in each layer within this file, 
rather than in the wells file.

Units are converted within the code.

Warnings stating that
FutureWarning: comparison to `None` will result in an elementwise object
comparison in the future
will appear. These can be ignored because they are being compared to
a single element only within an if statement. This is the intended use.

Save file name at last line of code.
"""

#SET OPTION TO HAVE TEMPERATURES AT ALL DEPTHS IN OUTPUT TABLE
#Change to 1 if you want all temperatures and stratigraphic information for each well in
#the output table. This computation is not highly time intensive, but will increase the
#output file size greatly.
AllTempsThicksConds = 1

#SET CONSTANTS:
#Set the value of the assumed basement conductivity
basement_conductivity = 2.7 #W/mK

#READ WELL DATA:
#Read in well data. Skip the first row (0) if it has units.
Wells = read_wells(file='ExampleInputTable_ForReservoirsIntervalTempData.csv',
                   skiprows=[])

#Delete nan rows from the read in data that may result from added rows at end of file:
if np.isnan(Wells['BasementDepth'][len(Wells)-1]):
        while np.isnan(Wells['BasementDepth'][len(Wells)-1]):
                Wells = Wells.ix[:(len(Wells)-2)]
#Delete nan columns if all values in the column are nan.
if all(np.isnan(Wells.ix[:,(len(Wells.columns)-1)])):
        while all(np.isnan(Wells.ix[:,(len(Wells.columns)-1)])):
            Wells = Wells.ix[:,:(len(Wells.columns)-1)]                

#Set the depth increment for the calculations (m)
#Note that temperatures at a specific depths will be linearly interpolated based on
#these depth increments.
depth_increment = 1. #m

#DECLARE CALCULATION DEPTH FIELD NAME
#Want this to be greater than the max depth to avoid complication in the output of the temperature information.
Wells['CalcField'] = round(max(Wells['ResDepthm'] + Wells['FmnThickm'])/depth_increment)*depth_increment + depth_increment
CalcField = 'CalcField'

#CONVERT UNITS:
#Convert from units of mW/m^2 to W/m^2
Wells['Qmantle'] /= 1000.
Wells['Qs'] /= 1000.
Wells['QsErr'] /= 1000.
#Convert ftom units of uW/m^3 to W/m^3
Wells['SedRadHeat'] /= 1.e6


#SET STORAGE LISTS:
'''Establish lists to store variables that will be returned
B - Log decrement of radiogenicity in basement (m)
Ab - Basement radiogenicity (uW/m**3)
StartDepth - Reservoir starting depth (m)
EndDepth - Reservoir ending depth (m)
Qs - Surface heat flow to well depth (mW/m**2)
BHT_diff - Difference in (predicted - measured) BHTs.
Temp2km - Temperature at 2 km (m)
Temp3km - Temperature at 3 km (m)
Temp4km - Temperature at 4 km (m)
Temp5km - Temperature at 5 km (m)
well_PSC - Suite of calculated conductivity and thickness information for each well.
'''
Bs = []
Abs = []
StartDepth = []
EndDepth = []
Temp_1 = []
TempBase = []
TempCalc = []
well_PSCs = []

#SET LOOP COUNTER VARIABLES: 
#Set the number of negative Ab values to 0, so that they may be counted.
num_neg_Ab = 0

#Counting for a progress bar in % of wells completed
count = 0.
#Set progress tracking starting value and reporting increment in percent.
progress_tracker = 10.
progress_increment = 10.

#Determine the number of columns needed for the temperature
NumTempCols = int(round(max(Wells['FmnThickm'])/depth_increment))+1

#Read in COSUNA stratigraphy files
CosunaKeys = [2701, 2801, 2901, 3001, 3101, 1701, 1801, 2101, 2201, 2301, 2401, 1201, 1301, 1702, 1802, 101, 201, 301, 801, 901, 1001, 401, 50101, 50102, 60101, 60102, 110101, 110102, 2102, 2202, 230201, 230202, 2402] 
CosunaFilenames = ['NY27.csv', 'NY28.csv', 'NY29.csv', 'NY30.csv', 'NY31.csv', 'PA17.csv', 'PA18.csv', 'PA21.csv', 'PA22.csv', 'PA23.csv', 'PA24.csv', 'MD12.csv', 'MD13.csv', 'KY17.csv', 'KY18.csv', 'WV1.csv', 'WV2.csv', 'WV3.csv', 'WV8.csv', 'WV9.csv', 'WV10.csv', 'VA4.csv', 'VA501.csv', 'VA502.csv', 'VA601.csv', 'VA602.csv', 'VA1101.csv', 'VA1102.csv', 'VA21.csv', 'VA22.csv', 'VA2301.csv', 'VA2302.csv', 'VA24.csv']
RomeKeys = [1701, 1801, 2101, 2201, 1702, 1802, 101, 201, 801, 901] 
RomeFilenames = ['PA17_RT.csv', 'PA18_RT.csv', 'PA21_RT.csv', 'PA22_RT.csv', 'KY17_RT.csv', 'KY18_RT.csv', 'WV1_RT.csv', 'WV2_RT.csv', 'WV8_RT.csv', 'WV9_RT.csv']
CosunaData = {}
RomeData = {}
read_strats(CosunaData, CosunaKeys, CosunaFilenames, 'Carter Conductivity')
read_strats(RomeData, RomeKeys, RomeFilenames, 'Carter Conductivity')

#BEGIN TEMP @ DEPTH CALCS:
#Loop through all wells to determine surface heat flow (Qs), and Temp @ Depths
for well in Wells.iterrows():
    #Assign the input data to row
    row = well[1]

    #Set starting depth for the calculations. This is the reservoir top, rounded to the nearest multiple of 10.
    #Must be greater than 0 to prevent nans. (m)
    start_depth = round(row['ResDepthm']/depth_increment)*depth_increment
    if start_depth == 0.:
            start_depth = depth_increment
            
    StartDepth.append(start_depth)
    
    #Set the maximum depth to calculate temperature at depth (m)
    MaxDepth = round(row['ResDepthm']/depth_increment)*depth_increment + round(row['FmnThickm']/depth_increment)*depth_increment
    if MaxDepth == start_depth:
            MaxDepth = start_depth + depth_increment
            
    EndDepth.append(MaxDepth)
    
    #Check that the heat flow is not -9999.
    if round(row['Qs']) != -10:
            #Store heat flow as variable.
            Qs = row['Qs']
            #Retrieve the calculation depth
            CalcDepth = row[CalcField]

            #If the basement depth is less than 0, set to 0 (surface).
            if row['BasementDepth'] < 0.:
                    row['BasementDepth'] = 0.

            #READ STRAT COLUMN:
            if row['ROME_ID'] == 1:
                    #Well is in the Rome Trough, use rome trough modified Cosuna column.
                    Strat = RomeData[row['COSUNA_ID']]
            else:
                    #Use regular Cosuna column
                    Strat = CosunaData[row['COSUNA_ID']]

            #Set the value of the log decrement in basement radiogenic heat production
            #The value of B may change based on data collected from the region of study. 
            if row['BasementDepth'] > 3000.:
                B = 13000. - row['BasementDepth']
            else:
                B = 10000.

            Bs.append(B)
    
            #For all wells:
            #First determine the conductivity to the calculation depth of interest. Could be an isopach.
            #using the partial strat column (PSC) function.
            this_PSC = PSC(Strat,row['BasementDepth'],basement_conductivity,row[CalcField])
            well_PSCs.append(this_PSC)
    
            #Calculate the RHP at the sediment-basement interface.
            Ab = (Qs - row['SedRadHeat']*row['BasementDepth'] - row['Qmantle']) \
                /(B*(1. - np.exp(-3.)))
    
            # Kludge! We are setting negative Ab's to zero here
            #FIXME: Reducing the Ab value and not adjusting another variable, such as mantle heat flow (below), upsets the heat balance.
            #As a result, temperatures at depth in the basement will be greater than they really are for these wells.
            if Ab < 0.: 
                Ab = 0.
                num_neg_Ab += 1
    
            #Store basement radiogenic heat production at the sediment-crustal interface in uW/m^3
            Abs.append(Ab*1.e6)

            #Begin Temperature Estimations
            Temps = []
            #Calculate temperature up until the calculation depth or the MaxDepth
            #This will always stop at or shallower than the calculation depth or MaxDepth. The 
            #'stop' value is not included in the increment, so a very small number is 
            #added so that the calculation depth or MaxDepth is included in the interval.
            if CalcDepth <= MaxDepth:
                    #Can estimate to the calculation depth.
                    Depths = np.arange(start_depth,
                                       CalcDepth+0.00001,  # keep CalcDepth if integer multiple of increment.
                                       depth_increment)
            else:
                    #CalcDepth is deeper than the maximum depth. Only go to the maximum depth.
                    Depths = np.arange(start_depth,
                                       MaxDepth+0.00001,  # keep MaxDepth if integer multiple of increment.
                                       depth_increment)
    
            if CalcDepth <= row['BasementDepth']:
                    for z in Depths:
                            #We're above the basement, and since we are setting "Depths" 
                            #up to the well depth, we must be estimating shallower than 
                            #the well depth, which is the "Upper" strat column:
                            K = this_PSC.EstimateKForUpperPartialStratColumn(z)
                                           
                            Temp = row['SurfTemp'] + SedTempEst(K, z, Qs, row['SedRadHeat'])

                            Temps.append(Temp)
                            
            else:
                    #CalcDepth is in the basement
                    for z in Depths:
                            if z <= row['BasementDepth']:
                                    #We're in the sediments, so use the "Upper" strat column, 
                                    #same as above:
                                    K = this_PSC.EstimateKForUpperPartialStratColumn(z)
                                           
                                    Temp = row['SurfTemp'] + SedTempEst(K, z, Qs, row['SedRadHeat'])

                                    Temps.append(Temp)
                
                            else:
                                    #We're in the basement, so this requires 2 conductivities. 
                                    #The first is Kc for the sediments (K here), and the second is
                                    #the basement conductivity, which is set above as a constant.
                                    K = this_PSC.EstimateKForUpperPartialStratColumn(row['BasementDepth'])
                
                                    #The z in SedTempEst is row['BasementDepth'] because we are
                                    #estimating to the extent of the sediment here. 
                                    #The z in BaseTempEst is (z-row['BasementDepth']),
                                    #as per function design.
                                    Temp = row['SurfTemp'] + \
                                           SedTempEst(K,
                                                      row['BasementDepth'],
                                                      Qs,
                                                      row['SedRadHeat']) + \
                                           BaseTempEst(basement_conductivity,
                                                       (z-row['BasementDepth']),
                                                       row['Qmantle'],
                                                       Ab,
                                                       B)

                                    Temps.append(Temp)
                
            #Calculate the temperature at the calc depth.
            if CalcDepth == 0.:
                    #Save the surface temperature. Print a warning.
                    print(i, "CalcDepth is 0")
                    TempCalc.append(row['SurfTemp'])
                    
            elif CalcDepth <= row['BasementDepth']:
                    calc = row['SurfTemp'] + \
                           SedTempEst(this_PSC.Kw,
                                      this_PSC.well_depth,
                                      Qs,
                                      row['SedRadHeat'])
                    TempCalc.append(calc)
                    
            else:
                    #Calc Depth is in the basement. 2 conductivities are needed: 
                    #the average in the sediment column (K_sed_col) 
                    #and the basement conductivity.
                    K_sed_col = this_PSC.EstimateKForUpperPartialStratColumn(row['BasementDepth'])

                    #The z in BaseTempEst is (CalcDepth-row['BasementDepth'])
                    calc = row['SurfTemp'] + \
                           SedTempEst(K_sed_col,
                                      row['BasementDepth'],
                                      Qs,
                                      row['SedRadHeat']) + \
                           BaseTempEst(basement_conductivity,
                                       (this_PSC.well_depth-row['BasementDepth']),
                                       row['Qmantle'],
                                       Ab,
                                       B)
                    
                    TempCalc.append(calc)


            #Calculate the temperature at the basement depth
            K_sed_col = this_PSC.EstimateKForUpperPartialStratColumn(row['BasementDepth'])

            #The z in BaseTempEst is (CalcDepth-row['BasementDepth'])
            if row['BasementDepth'] == 0.:
                    TempBase.append(row['SurfTemp'])
            else:
                    BaseCalc = row['SurfTemp'] + \
                               SedTempEst(K_sed_col,
                                          row['BasementDepth'],
                                          Qs,
                                          row['SedRadHeat'])
                    TempBase.append(BaseCalc)

    
            #Calculate the temperatures up until the basement
            #Use this loop for estimating temperature at depths deeper than the 
            #calc depth, but shallower than the basement.
            if Depths == None:
                    #CalcDepth is less than the start_depth. Check if the basement is less than the max depth.
                    if row['BasementDepth'] <= MaxDepth:
                            #Can estimate to the basement. This could be None if the basement is also less than the start depth.
                            Depths2 = np.arange(start_depth,
                                               row['BasementDepth']+0.00001,  # keep CalcDepth if integer multiple of increment.
                                               depth_increment)
                    else:
                            #Basement is deeper than the maximum depth. Only go to the maximum depth.
                            Depths2 = np.arange(start_depth,
                                               MaxDepth+0.00001,  # keep MaxDepth if integer multiple of increment.
                                               depth_increment)
                    
                    for z2 in Depths2:
                                    K = this_PSC.EstimateKForUpperPartialStratColumn(z2)
    
                                    Temp = row['SurfTemp'] + \
                                           SedTempEst(K, 
                                                     z2, 
                                                     Qs, 
                                                     row['SedRadHeat']) + \
                                    Temps.append(Temp)

            elif Depths[-1] == MaxDepth:
                    #Do Nothing. Already calculated temperature to the maximum desired depth
                    Depths2 = None
            else:
                    #There are more depths before MaxDepth.
                    #Check if the last increment was deeper than the basement.
                    if (Depths[-1]+depth_increment) <= row['BasementDepth']:
                           #Calculate the temperature up until the basement.
                            if row['BasementDepth'] <= MaxDepth:
                                    #Top of Basement is shallower than MaxDepth.
                                    #Calculate to Top of Basement.
                                    Depths2 = np.arange(Depths[-1]+depth_increment,
                                                        row['BasementDepth']+0.00001,
                                                        depth_increment)
                            else:
                                    #Basement is deeper than the maximum depth.
                                    #Only calculate to MaxDepth.
                                    Depths2 = np.arange(Depths[-1]+depth_increment,
                                                        MaxDepth+0.00001,
                                                        depth_increment)
                    
                            for z2 in Depths2:
                                    Kw, Kss, Kc = this_PSC.EstimateKForLowerPartialStratColumn(z2)
    
                                    Temp = row['SurfTemp'] + \
                                           SedTempEst(Kw, 
                                                     this_PSC.well_depth, 
                                                     Qs, 
                                                     row['SedRadHeat']) + \
                                           SedTempEst(Kss, 
                                                     (z2 - this_PSC.well_depth), 
                                                     (Qs - row['SedRadHeat']*this_PSC.well_depth), 
                                                     row['SedRadHeat'])

                                    Temps.append(Temp)

                    else:
                            #The case when MaxDepth has not been reached, and the
                            #last depth in Depths was greater than the basement depth
                            #because the calc depth was in the basement.
                            #Depths 2 not needed, so set to None.
                            Depths2 = None


            #Use this loop once estimating in the basement.
            if Depths == None:
                    #CalcDepth Less than start_depth
                    if Depths2 == None:
                            #Basement is also less than start_depth.
                            Depths3 = np.arange(start_depth, 
                                                MaxDepth+0.00001,
                                                depth_increment)
                            if row['BasementDepth'] == 0.:
                                    #This is just a basement calculation.
                                    for z3 in Depths3:
                                            Temp = row['SurfTemp'] + \
                                                   BaseTempEst(basement_conductivity, # In the basement
                                                              (z3-this_PSC.basement_depth), 
                                                              (row['Qmantle']), 
                                                              Ab, 
                                                              B)

                                            Temps.append(Temp)
                            else:
                                    #There are some sediments above basement
                                    K = this_PSC.EstimateKForUpperPartialStratColumn(row['BasementDepth'])
                                    for z3 in Depths3:
                                            Temp = row['SurfTemp'] + \
                                                   SedTempEst(K, # Below surface in remainder of sediments
                                                              this_PSC.basement_depth, 
                                                              Qs, 
                                                              row['SedRadHeat'])  + \
                                                   BaseTempEst(basement_conductivity, # In the basement
                                                              (z3-this_PSC.basement_depth), 
                                                              (row['Qmantle']), 
                                                              Ab, 
                                                              B)

                                            Temps.append(Temp)
                    else:
                            #There are sediment layers above this.
                            if Depths2[-1] == MaxDepth:
                                    Depths3 = None
                            else:
                                    K = this_PSC.EstimateKForUpperPartialStratColumn(row['BasementDepth'])
                                    Depths3 = np.arange(Depths2[-1],
                                                        MaxDepth+0.00001,
                                                        depth_increment)
                                    for z3 in Depths3:
                                           Temp = row['SurfTemp'] + \
                                           SedTempEst(K, # Below surface in remainder of sediments
                                                      this_PSC.basement_depth, 
                                                      Qs, 
                                                      row['SedRadHeat'])  + \
                                           BaseTempEst(basement_conductivity, # In the basement
                                                      (z3-this_PSC.basement_depth), 
                                                      (row['Qmantle']), 
                                                      Ab, 
                                                      B)

                                    Temps.append(Temp)
                                    
            elif Depths[-1] == MaxDepth:
                    #Do nothing. Already calculated to the maximum depth.
                    Depths3 = None
            elif Depths2 == None:
                    if Depths[-1] == MaxDepth:
                            #Do nothing. Already calculated to the maximum depth.
                            Depths3 = None
                    else:              
                            #Calculate Depths 3
                            #Have not calculated to the MaxDepth. Continue to MaxDepth.
                            #Use the last element in Depths as the starting point
                            Depths3 = np.arange(Depths[-1]+depth_increment, 
                                                MaxDepth+0.00001,
                                                depth_increment)
                    
                            #Assign conductivities for calculations.  
                            if CalcDepth < row['BasementDepth']:
                                Kss_column = (1./((this_PSC.basement_depth/this_PSC.Kc - this_PSC.well_depth/this_PSC.Kw)/(this_PSC.basement_depth - this_PSC.well_depth)))
                            else:
                                #CalcDepth penetrates the basement. This is Kc 
                                Kss_column = this_PSC.EstimateKForUpperPartialStratColumn(row['BasementDepth'])

                            if CalcDepth < row['BasementDepth']:
                                #CalcDepth shallower than basement.
                                for z3 in Depths3:
                                    Temp = row['SurfTemp'] + \
                                           SedTempEst(this_PSC.Kw,  # To CalcDepth in sediments
                                                      this_PSC.well_depth, 
                                                      Qs, 
                                                      row['SedRadHeat']) + \
                                           SedTempEst(Kss_column, # Below CalcDepth in remainder of sediments
                                                      (this_PSC.basement_depth - this_PSC.well_depth), 
                                                      (Qs - row['SedRadHeat']*this_PSC.well_depth), 
                                                      row['SedRadHeat'])  + \
                                           BaseTempEst(basement_conductivity, # In the basement
                                                      (z3-this_PSC.basement_depth), 
                                                      (row['Qmantle']), 
                                                      Ab, 
                                                      B)

                                    Temps.append(Temp)
                    
                            else:
                                #CalcDepth is deeper than the basement
                                for z3 in Depths3:
                                    Temp = row['SurfTemp'] + \
                                           SedTempEst(Kss_column,  # The entire sedimentary column
                                                      row['BasementDepth'], 
                                                      Qs, 
                                                      row['SedRadHeat']) + \
                                           BaseTempEst(basement_conductivity, 
                                                      (z3-this_PSC.basement_depth), 
                                                      (row['Qmantle']), 
                                                      Ab, 
                                                      B)  # The basement segment of the well

                                    Temps.append(Temp)
            
            elif Depths2[-1] == MaxDepth:
                            #Do nothing. Already calculated to maximum depth.
                            Depths3 = None
            else:
                    #Have not calculated to the MaxDepth. Continue to MaxDepth.
                    if CalcDepth <= row['BasementDepth']:
                            #Use the last element in Depths2 as the starting point
                            Depths3 = np.arange(Depths2[-1]+depth_increment, 
                                                MaxDepth+0.00001,
                                                depth_increment)
                    else: 
                            #Use the last element in Depths as the starting point
                            Depths3 = np.arange(Depths[-1]+depth_increment, 
                                                MaxDepth+0.00001,
                                                depth_increment)

                    #Assign conductivities for calculations
                    if CalcDepth < row['BasementDepth']:
                        #This is Kss, below the CalcDepth in the sediments.
                        Kss_column = (1./((this_PSC.basement_depth/this_PSC.Kc - this_PSC.well_depth/this_PSC.Kw)/(this_PSC.basement_depth - this_PSC.well_depth)))
                    else:
                        #CalcDepth penetrates the basement. This is Kc
                        Kss_column = this_PSC.EstimateKForUpperPartialStratColumn(row['BasementDepth'])

                    #Calculate temperature to the maximum depth.    
                    if CalcDepth < row['BasementDepth']:
                        for z3 in Depths3:
                            Temp = row['SurfTemp'] + \
                                   SedTempEst(this_PSC.Kw,  # To CalcDepth in sediments
                                              this_PSC.well_depth, 
                                              Qs, 
                                              row['SedRadHeat']) + \
                                   SedTempEst(Kss_column, # Below CalcDepth in remainder of sediments
                                              (this_PSC.basement_depth - this_PSC.well_depth), 
                                              (Qs - row['SedRadHeat']*this_PSC.well_depth), 
                                              row['SedRadHeat'])  + \
                                   BaseTempEst(basement_conductivity, # In the basement
                                              (z3-this_PSC.basement_depth), 
                                              (row['Qmantle']), 
                                              Ab, 
                                              B)

                            Temps.append(Temp)
            
                    else:
                        #CalcDepth is deeper than the basement
                        for z3 in Depths3:
                            Temp = row['SurfTemp'] + \
                                   SedTempEst(Kss_column,  # The entire sedimentary column
                                              row['BasementDepth'], 
                                              Qs, 
                                              row['SedRadHeat']) + \
                                   BaseTempEst(basement_conductivity, 
                                              (z3-this_PSC.basement_depth), 
                                              (row['Qmantle']), 
                                              Ab, 
                                              B)  # The basement segment of the well...

                            Temps.append(Temp)

            #Set up depths for plotting
            if Depths == None:
                    if Depths2 == None:
                            all_depths = Depths3
                    else:
                            all_depths = np.concatenate((Depth2,Depths3))
            elif Depths3 == None:
                    if Depths2 == None:
                            all_depths = Depths
                    else:
                            all_depths = np.concatenate((Depths,Depths2))
            else:
                    if Depths2 == None:
                            all_depths = np.concatenate((Depths,Depths3))
                    else:
                            all_depths = np.concatenate((Depths,Depths2,Depths3))        

            if AllTempsThicksConds == 1:
                    while len(Temps) < NumTempCols:
                            Temps.append(-9999.)
                    Temp_1.append(Temps)
    
            #Clear Variables for next loop run
            Strat = None
            B = None
            this_PSC = None
            Qs = None
            Ab = None
            calc = None
            BaseCalc
            K = None
            Kw = None
            Kss = None
            Kc = None
            Kss_column = None
            K_sed_col = None
            all_depths = None
            Depths = None
            Depths2 = None
            Depths3 = None
            Temp = None
    else:
            #Heat flow is not known here. Set all lists to -9999 here.
            Bs.append(-9999.)
            Abs.append(-9999.)
            TempBase.append(-9999.)
            TempCalc.append(-9999.)
            well_PSCs.append(-9999.)

            if AllTempsThicksConds == 1:
                    Temp_1.append([-9999.]*NumTempCols)

    #Send Progress Update in 10% increments as a percent of total wells run:
    count += 1.
    progress = count/len(Wells['BasementDepth'])*100
    if progress >= progress_tracker:
            print progress_tracker
            progress_tracker += progress_increment
    

pd.set_option('float_format', '{:10,.4g}'.format)

#STORE CALCULATED DATA
#Convert Qmantle and SedRadHeat back to original units
Wells['Qmantle'] *= 1000.
Wells['Qs'] *= 1000.
Wells['QsErr'] *= 1000.
Wells['SedRadHeat'] *= 1.e6
# Add new columns for, Ab, and the temps@depth (PSC) to Wells.
idx = Wells.index
Wells['Ab'] = pd.Series(Abs, index=idx)
Wells['B'] = pd.Series(Bs, index=idx)
Wells['StartDepth'] = pd.Series(StartDepth, index=idx)
Wells['EndDepth'] = pd.Series(EndDepth, index=idx)
#Store well-specific information. Use well_PSCs[idx].__dict__ to extract other information
Wells['TempBase'] = pd.Series(TempBase, index=idx)
Wells['TempCalc'] = pd.Series(TempCalc, index=idx)
Kw = []
Kc = []

#Extract information from the PSC stored data.
for i in idx:
        if well_PSCs[i] != -9999.:
                Kw.append(well_PSCs[i].Kw)
                Kc.append(well_PSCs[i].Kc)
        else:
                Kw.append(-9999.)
                Kc.append(-9999.)
Wells['KCalc'] = pd.Series(Kw, index=idx)
Wells['Kc'] = pd.Series(Kc, index=idx)

if AllTempsThicksConds == 1:
        print('Saving thicknesses, conductivities, and temperatures.')
        #Determine the length of the thickness and conductivity lists
        for i in range(len(CosunaData)):
                if i == 0:
                        arrlen = len(CosunaData[CosunaKeys[i]])
                else:
                        lenrow = len(CosunaData[CosunaKeys[i]])
                        arrlen = max(arrlen, lenrow)
        #Then check the RomeData columns
        for j in range(len(RomeData)):
                arrlen = max(arrlen, len(RomeData[RomeKeys[j]]))

        #Initialize the lists of Thicks and Conds to the column length of the max columns in Cosuna columns
        Thicks = [None]*arrlen
        Conds = [None]*arrlen

        #Extract thickness and conductivity info.
        count = 0
        for i in idx:
                if well_PSCs[i] != -9999.:
                        #Fill in the array of lithologic thickness and conductivity of each well
                        if count == 0:
                                #Fill in the initialized row
                                Thicks[0:len(well_PSCs[i].Whole_Sediment_Column_Thick)] = well_PSCs[i].Whole_Sediment_Column_Thick
                                Conds[0:len(well_PSCs[i].Whole_Sediment_Column_Cond)] = well_PSCs[i].Whole_Sediment_Column_Cond
                                count = 1
                        else:
                                #Add a new row of None
                                Thicks = np.vstack([Thicks, [None]*arrlen])
                                Conds = np.vstack([Conds, [None]*arrlen])
                                #Add data to this row.
                                Thicks[i,0:len(well_PSCs[i].Whole_Sediment_Column_Thick)] = well_PSCs[i].Whole_Sediment_Column_Thick
                                Conds[i,0:len(well_PSCs[i].Whole_Sediment_Column_Cond)] = well_PSCs[i].Whole_Sediment_Column_Cond
                else:
                        #Fill in the array of lithologic thickness and conductivity of each well
                        if count == 0:
                                #Fill in the initialized row
                                Thicks[0:1] = well_PSCs[i]
                                Conds[0:1] = well_PSCs[i]
                                count = 1
                        else:
                                #Add a new row of None
                                Thicks = np.vstack([Thicks, [None]*arrlen])
                                Conds = np.vstack([Conds, [None]*arrlen])
                                #Add data to this row.
                                Thicks[i,0:1] = well_PSCs[i]
                                Conds[i,0:1] = well_PSCs[i]

        #Extract the information to the well database.
        #Make column names. First find an index with non -9999 data.
        for h in idx:
                if len(Temp_1[h]) > 1:
                        use = h
                        break
                        
        ColNamesTemps = ["T" + str(k) for k in (range(len(Temp_1[use]))[1:(len(range(len(Temp_1[use])))+1)]+[len(Temp_1[use])])]
        ColNamesThicks = ["Layer" + str(k) for k in (range(len(Thicks[use]))[1:(len(range(len(Thicks[use])))+1)]+[len(Thicks[use])])]
        ColNamesConds = ["Cond" + str(k) for k in (range(len(Conds[use]))[1:(len(range(len(Conds[use])))+1)]+[len(Conds[use])])]
        #Extract the information
        for j in range(len(Thicks[use])):
                Wells[ColNamesThicks[j]] = None
        for j in range(len(Conds[use])):
                Wells[ColNamesConds[j]] = None
        for j in range(len(Temp_1[use])):
                Wells[ColNamesTemps[j]] = None
        for j in range(len(Temp_1[use])):
                Wells[ColNamesTemps[j]] = np.array(Temp_1)[:,j]
        for j in range(len(Thicks[use])):
                Wells[ColNamesThicks[j]] = np.array(Thicks)[:,j]
        for j in range(len(Conds[use])):
                Wells[ColNamesConds[j]] = np.array(Conds)[:,j]

#PRINT DATA:
#print number of negative Ab values
print num_neg_Ab

#SAVE DATA:
Wells.to_csv('ExampleOutputTable_ForReservoirsIntervalTempData_1m.csv')
