from nose import with_setup
from nose.tools import assert_equals, assert_raises, raises, assert_almost_equal
import pandas as pd
import numpy as np
from TempAtDepthIterator import TempAtDepth

Temps = None
Depths = None
DesiredDepth = None
                        
def TempAtDepth_setup_function():
    """Setup a few constant thingies for testing TempAtDepthIterator"""
    global Temps
    global Depths
    global DesiredDepth
    
    Temps = [60., 70., 80., 90., 100.]  # C
    Depths = [1000., 1300., 1800., 1900., 2000.] # m
    DesiredDepth = 1500.
     
def teardown_function():
    """Teardown the matching constants from setup..."""
    global Temps
    global Depths
    global DesiredDepth

    del Temps
    del Depths
    del DesiredDepth
    
    
@with_setup(TempAtDepth_setup_function,teardown_function)
def testConvertDepthsToArray():
    global Temps
    global Depths
    global DesiredDepth
    
    DepthsArray = np.array(Depths)
    assert_equals(DepthsArray[0], Depths[0])

@with_setup(TempAtDepth_setup_function,teardown_function)
def testMinus9999ForDepthOutOfRange():
    global Temps
    global Depths
    global DesiredDepth

    DesiredDepth = 2100.
    answer = TempAtDepth(Temps, DesiredDepth, Depths)
    assert_equals(-9999.,answer)
    
    DesiredDepth = 900.
    answer = TempAtDepth(Temps, DesiredDepth, Depths)
    assert_equals(-9999.,answer)
    
@with_setup(TempAtDepth_setup_function,teardown_function)
def testIndexGreaterThanDesiredDepth():
    global Temps
    global Depths
    global DesiredDepth

    DesiredDepth = 1600.

    answer = TempAtDepth(Temps, DesiredDepth, Depths)
    correct_answer = 76.
    assert_equals(correct_answer,answer)

@with_setup(TempAtDepth_setup_function,teardown_function)
def testIndexLessThanDesiredDepth():
    global Temps
    global Depths
    global DesiredDepth

    answer = TempAtDepth(Temps, DesiredDepth, Depths)
    correct_answer = 74.
    assert_equals(correct_answer,answer)

@with_setup(TempAtDepth_setup_function,teardown_function)
def testFirstDepthIndexIsClosest():
    global Temps
    global Depths
    global DesiredDepth

    DesiredDepth = 1000.
    answer = TempAtDepth(Temps, DesiredDepth, Depths)
    correct_answer = 60.
    assert_equals(correct_answer,answer)

    DesiredDepth = 1120.
    answer = TempAtDepth(Temps, DesiredDepth, Depths)
    correct_answer = 64.
    assert_equals(correct_answer,answer)

@with_setup(TempAtDepth_setup_function,teardown_function)
def testLastDepthIndexIsClosest():
    global Temps
    global Depths
    global DesiredDepth

    DesiredDepth = 2000.
    answer = TempAtDepth(Temps, DesiredDepth, Depths)
    correct_answer = 100.
    assert_equals(correct_answer,answer)

    DesiredDepth = 1970.
    answer = TempAtDepth(Temps, DesiredDepth, Depths)
    correct_answer = 97.
    assert_equals(correct_answer,answer)
   
if __name__ == '__main__':
    import nose
    nose.run(argv=[__file__, '--with-doctest', '-v'])
